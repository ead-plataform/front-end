## Front-End Plataforma - EAD

<p>
Esse aplicação foi desenvolvida para aperfeiçoar meus conhecimentos em React
</p>

<br />

## Status do Projeto

<h4> 
	🚧  Progresso 🚀 Em construção...  🚧
</h4>

<br/>

## Funcionalidades

-   [x] Cadastro do aluno
-   [x] Login do aluno
-   [x] Recuperação de senha
-   [x] Alterar informações Perfil
-   [x] Ativação da conta
-   [x] Listar Cursos
-   [x] Listar Modulos
-   [x] Listar Aulas
-   [x] Progresso do Aluno
-   [x] Download de PDF na plataforma
-   [x] Administração Admin
-   [x] Cadastrar Novos Cursos
-   [x] Cadastrar Novos Modulos
-   [x] Cadastrar Novas Aulas
-   [x] Bloqueio de acesso simultâneo na plataforma

<br />

## Tecnologia usadas

-   ReactJs
-   Styled-Components
-   Eslint
-   Prettier

## Demonstração da Aplicação

<a href="https://cerebronerd.com.br/" target="blank">Aplicação</a>
