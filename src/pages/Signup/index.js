import { Link, useHistory } from 'react-router-dom';
import { useState } from 'react';

import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import PersonIcon from '@material-ui/icons/Person';

import { SigninContainer } from '../Signin/signin-syle';
import { BannerAuth } from '../../components/BannerAuth';
import { Button } from '../../components/Button';
import Loading from '../../components/Load';
import AlertMessage from '../../components/AlertMessage';

import api from '../../services/api';

const Signup = () => {
    const history = useHistory();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [focusedName, setFocusedName] = useState(false);
    const [focused, setFocused] = useState(false);
    const [focusedPass, setFocusedPass] = useState(false);
    const [viewed, setViewed] = useState(false);
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState({});
    const [messageAlert, setMessageAlert] = useState('');
    const [checked, setChecked] = useState({ value: 'M' });

    const handleView = () => {
        setViewed(!viewed);
    };
    const handleCheckedGenero = (event) => {
        setChecked(event.target);
    };

    const handleForm = async (event) => {
        event.preventDefault();
        setLoading(true);

        if (password.length < 6) {
            setLoading(false);
            setMessageAlert(
                'A senha deve ter pelo menos 6 caracteres. Tente inserir outra senha.'
            );
            setMessage({
                variant: 'filled',
                severity: 'error',
                show: true,
            });
            setTimeout(() => {
                setMessage({ ...message, show: false });
            }, 4000);
        }

        if (name && email && password.length >= 6) {
            const signup = await api.signup(
                { name, email, password, sexo: checked.value },
                '/signup'
            );
            if (signup.token) {
                history.push('/signup/confirmation');
            }
            if (signup.error) {
                setLoading(false);
                setMessageAlert(signup.error);
                setMessage({
                    variant: 'filled',
                    severity: 'error',
                    show: true,
                });
                setTimeout(() => {
                    setMessage({ ...message, show: false });
                }, 2500);
            }
        }
    };

    document.title = 'Registre-se';

    return (
        <>
            <AlertMessage
                variant={message.variant}
                severity={message.severity}
                show={message.show}
            >
                {messageAlert}
            </AlertMessage>
            <BannerAuth />
            <SigninContainer>
                <form className="auth" method="POST" onSubmit={handleForm}>
                    <div className="auth--areas-banner height--flex">
                        <h1 className="auth--action-span ">Registre-se</h1>

                        <p className="auth--action-register">
                            Já está registrado? Faça&nbsp;
                            <Link to="/" className="auth-areas-signin">
                                Login
                            </Link>
                            &nbsp; agora mesmo
                        </p>
                    </div>
                    <div className="auth--ares-forms">
                        <div
                            className="auth--areas-content"
                            style={{
                                borderColor: focusedName && '#595bf9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <PersonIcon
                                    style={{
                                        color: focusedName
                                            ? '#595bf9'
                                            : 'd9d9d9',
                                    }}
                                    fontSize="small"
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type="text"
                                    name="name"
                                    required
                                    placeholder="Seu nome"
                                    className="input--info"
                                    value={name}
                                    onChange={(e) =>
                                        setName(e.currentTarget.value)
                                    }
                                    onFocus={() => setFocusedName(true)}
                                    onBlur={() => setFocusedName(false)}
                                    autoComplete="off"
                                />
                            </div>
                        </div>
                        <div
                            className="auth--areas-content"
                            style={{
                                borderColor: focused && '#595bf9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <EmailOutlinedIcon
                                    style={{
                                        color: focused ? '#595bf9' : 'd9d9d9',
                                    }}
                                    fontSize="small"
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type="email"
                                    name="email"
                                    required
                                    placeholder="Seu e-mail"
                                    className="input--info"
                                    value={email}
                                    onChange={(e) =>
                                        setEmail(e.currentTarget.value)
                                    }
                                    onFocus={() => setFocused(true)}
                                    onBlur={() => setFocused(false)}
                                    autoComplete="off"
                                />
                            </div>
                        </div>
                        <div className="login--content-genero">
                            <div className="login--content-type">
                                <span>Gênero?</span>
                            </div>
                            <div className="login--genero-groups">
                                <div className="login--genero-options">
                                    <label htmlFor="masc">
                                        Masculino
                                        <input
                                            type="radio"
                                            name="sexo"
                                            required
                                            defaultChecked
                                            id="masc"
                                            value="M"
                                            onChange={handleCheckedGenero}
                                        />
                                    </label>
                                </div>
                                <div
                                    className="login--genero-options"
                                    htmlFor="fem"
                                >
                                    {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                                    <label htmlFor="fem">
                                        Feminino
                                        <input
                                            type="radio"
                                            name="sexo"
                                            required
                                            id="fem"
                                            value="F"
                                            onChange={handleCheckedGenero}
                                        />
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div
                            className="auth--areas-contentpass space--bottom"
                            style={{
                                borderColor: focusedPass
                                    ? '#595bf9'
                                    : '#d9d9d9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <LockOutlinedIcon
                                    fontSize="small"
                                    style={{
                                        color: focusedPass
                                            ? '#595bf9'
                                            : '#d9d9d9',
                                    }}
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type={viewed ? 'text' : 'password'}
                                    name="password"
                                    required
                                    placeholder="Sua senha"
                                    className="input--info"
                                    value={password}
                                    onChange={(e) =>
                                        setPassword(e.currentTarget.value)
                                    }
                                    onFocus={() => setFocusedPass(true)}
                                    onBlur={() => setFocusedPass(false)}
                                />
                            </div>
                            <div
                                className="auth--areas-view"
                                onClick={handleView}
                                role="presentation"
                            >
                                {viewed && (
                                    <VisibilityOffIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                                {!viewed && (
                                    <VisibilityOutlinedIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="auth--areas-button">
                        <div className="auth--areas-buttoncontent">
                            <Button
                                color="#595bf9"
                                style={{
                                    cursor: loading ? 'not-allowed' : 'pointer',
                                }}
                            >
                                {loading ? <Loading /> : 'Registre-se'}
                            </Button>
                        </div>
                    </div>
                </form>
            </SigninContainer>
        </>
    );
};

export default Signup;
