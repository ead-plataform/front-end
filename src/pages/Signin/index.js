import jwt from 'jwt-decode';
import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import { ExitToAppTwoTone, Payment } from '@material-ui/icons';
import { SigninContainer } from './signin-syle';
import { BannerAuth } from '../../components/BannerAuth';
import { Button } from '../../components/Button';
import Loading from '../../components/Load';
import AlertMessage from '../../components/AlertMessage';

import api from '../../services/api';
import { Login, doLogin } from '../../helpers/auth-user';
import { AppUserValue } from '../../contexts/UserContext';

const Signin = () => {
    const history = useHistory();
    const [state, dispatch] = AppUserValue(); // eslint-disable-line
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [focused, setFocused] = useState(false);
    const [focusedPass, setFocusedPass] = useState(false);
    const [viewed, setViewed] = useState(false);
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState({});
    const [messageAlert, setMessageAlert] = useState('');

    const isLogged = doLogin();

    if (isLogged) {
        history.push('/app');
    }

    const handleView = () => {
        setViewed(!viewed);
    };

    const handleForm = async (event) => {
        event.preventDefault();
        setLoading(true);
        if (email && password) {
            const signin = await api.signin({ email, password }, '/signin');
            const decryptToken = signin.token && jwt(signin.token);
            if (signin.token) {
                if (!decryptToken.status) {
                    setLoading(false);
                    setMessageAlert(
                        'Este e-mail ainda não foi verificado. Verifique sua caixa de entrada.'
                    );
                    setMessage({
                        variant: 'filled',
                        severity: 'error',
                        show: true,
                    });
                    setTimeout(() => {
                        setMessage({ show: false });
                    }, 3500);
                } else {
                    dispatch({ type: 'addid', id: decryptToken.id });
                    dispatch({
                        type: 'addname',
                        name: decryptToken.name,
                    });
                    dispatch({
                        type: 'addprofile',
                        profile: `${decryptToken.profile}`,
                    });
                    dispatch({
                        type: 'addviewfree',
                        view_free: decryptToken.view_free_time,
                    });
                    dispatch({
                        type: 'addprofile',
                        profile: `${decryptToken.profile}`,
                    });
                    dispatch({
                        type: 'addemail',
                        email: decryptToken.email,
                    });
                    dispatch({
                        type: 'addzipcode',
                        zipcode: decryptToken.zipcode,
                    });
                    dispatch({
                        type: 'addaddress',
                        address: decryptToken.address,
                    });
                    dispatch({
                        type: 'addnumber',
                        number: decryptToken.number,
                    });
                    dispatch({
                        type: 'addcity',
                        city: decryptToken.city,
                    });
                    dispatch({
                        type: 'addbairro',
                        bairro: decryptToken.district,
                    });
                    dispatch({
                        type: 'addstate',
                        state: decryptToken.state,
                    });
                    dispatch({
                        type: 'addphone',
                        phone: decryptToken.phone,
                    });
                    dispatch({
                        type: 'addpayment',
                        payment: decryptToken.payment,
                    });
                    if (
                        new Date(decryptToken.view_free_time) < new Date() &&
                        decryptToken.payment === false
                    ) {
                        setLoading(false);
                        setMessageAlert(
                            'Seu tempo gratuito na plataforma expirou!'
                        );
                        setMessage({
                            variant: 'filled',
                            severity: 'warning',
                            show: true,
                        });
                        setTimeout(() => {
                            history.push('/');
                            setMessage({ show: false });
                        }, 4500);
                    } else {
                        Login(signin.token);
                        window.location.href = '/app';
                    }
                }
            }
            if (
                signin.token &&
                new Date(decryptToken.view_free_time) < new Date()
            ) {
                setLoading(false);
                setMessageAlert('Seu tempo gratuito na plataforma expirou!');
                setMessage({
                    variant: 'filled',
                    severity: 'warning',
                    show: true,
                });
                setTimeout(() => {
                    history.push('/');
                    setMessage({ show: false });
                }, 3500);
            }
            if (signin.error) {
                setLoading(false);
                setMessageAlert(signin.error);
                setMessage({
                    variant: 'filled',
                    severity: 'error',
                    show: true,
                });
                setTimeout(() => {
                    setMessage({ show: false });
                }, 2500);
            }
        } else {
            setLoading(false);
            setMessageAlert('Preencha corretamente os campos!');
            setMessage({ variant: 'filled', severity: 'error', show: true });
            setTimeout(() => {
                setMessage({ severity: 'error', show: false });
            }, 2500);
        }
    };

    document.title = 'Login';

    return (
        <>
            <AlertMessage
                variant={message.variant}
                severity={message.severity}
                show={message.show}
            >
                {messageAlert}
            </AlertMessage>
            <BannerAuth />
            <SigninContainer>
                <form className="auth" method="POST" onSubmit={handleForm}>
                    <div className="auth--areas-banner">
                        <img
                            src="/assets/image/profile.svg"
                            alt="profile"
                            className="auth--areas-profile"
                        />
                        <h1 className="auth--action-span">Fazer Login</h1>
                    </div>
                    <div className="auth--ares-forms">
                        <div
                            className="auth--areas-content"
                            style={{
                                borderColor: focused && '#595bf9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <EmailOutlinedIcon
                                    style={{
                                        color: focused ? '#595bf9' : 'd9d9d9',
                                    }}
                                    fontSize="small"
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type="email"
                                    name="email"
                                    required
                                    placeholder="exemplo@gmail.com"
                                    className="input--info"
                                    value={email}
                                    onChange={(e) =>
                                        setEmail(e.currentTarget.value)
                                    }
                                    onFocus={() => setFocused(true)}
                                    onBlur={() => setFocused(false)}
                                />
                            </div>
                        </div>
                        <div
                            className="auth--areas-contentpass"
                            style={{
                                borderColor: focusedPass
                                    ? '#595bf9'
                                    : '#d9d9d9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <LockOutlinedIcon
                                    fontSize="small"
                                    style={{
                                        color: focusedPass
                                            ? '#595bf9'
                                            : '#d9d9d9',
                                    }}
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type={viewed ? 'text' : 'password'}
                                    name="password"
                                    required
                                    placeholder="Informe sua senha"
                                    className="input--info"
                                    value={password}
                                    onChange={(e) =>
                                        setPassword(e.currentTarget.value)
                                    }
                                    onFocus={() => setFocusedPass(true)}
                                    onBlur={() => setFocusedPass(false)}
                                />
                            </div>
                            <div
                                className="auth--areas-view"
                                onClick={handleView}
                                role="presentation"
                            >
                                {viewed && (
                                    <VisibilityOffIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                                {!viewed && (
                                    <VisibilityOutlinedIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="auth--areas-forgot">
                        <div className="auth--areas-action">
                            <Link to="forgot_password" href="/forgot_password">
                                Esqueceu sua senha?
                            </Link>
                        </div>
                    </div>
                    <div className="auth--areas-button">
                        <div className="auth--areas-buttoncontent">
                            <Button
                                color="#595bf9"
                                style={{
                                    cursor: loading ? 'not-allowed' : 'pointer',
                                }}
                            >
                                {loading ? <Loading /> : 'Login'}
                            </Button>
                        </div>
                    </div>
                    <div className="auth--areas-register">
                        <div className="auth--areas-registeraction">
                            <span className="auth--areas-span">
                                Não Tem Conta?
                            </span>
                            &nbsp;
                            <Link to="/signup" className="register-action">
                                Registre-se
                            </Link>
                        </div>
                    </div>
                </form>
            </SigninContainer>
        </>
    );
};

export default Signin;
