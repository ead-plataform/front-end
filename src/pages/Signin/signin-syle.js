import styled from 'styled-components';

export const SigninContainer = styled.div`
    width: 50%;
    display: flex;
    justify-content: center;

    .auth {
        width: 100%;
        max-width: 500px;
        height: max-content;
        margin-top: 70px;
    }
    .auth--areas-banner {
        display: flex;
        height: 130px;
        flex-direction: column;
        align-items: center;
        justify-content: space-between;
        margin-bottom: 30px;
    }
    .auth--areas-profile {
        width: 100%;
        max-width: 80px;
    }
    .auth--action-span {
        font-size: 1.3rem;
        text-transform: uppercase;
    }
    .auth--ares-forms {
        display: flex;
        flex-direction: column;
        align-items: center;
    }
    .auth--areas-content {
        display: grid;
        width: 100%;
        max-width: 340px;
        grid-template-columns: 7% 93%;
        padding: 5px 0;
        border-bottom: 2px solid #d9d9d9;
        margin-bottom: 25px;
    }
    .auth--areas-i,
    .auth--areas-view {
        display: flex;
        align-items: center;
        user-select: none;
    }
    .auth--areas-view {
        cursor: pointer;
    }
    .auth--areas-input {
        height: 45px;
    }
    .auth--areas-input .input--info {
        width: 100%;
        height: 100%;
        border: none;
        outline: none;
        padding: 0.5rem 0 0.5rem 0.7rem;
        font-family: Roboto, sans-serif;
    }
    .auth--areas-contentpass {
        display: grid;
        grid-template-columns: 7% 86% 7%;
        width: 100%;
        max-width: 340px;
        padding: 5px 0;
        border-bottom: 2px solid #d9d9d9;
    }
    .auth--areas-forgot {
        display: flex;
        justify-content: center;
    }
    .auth--areas-action {
        width: 100%;
        max-width: 340px;
        padding: 16px 0;
        display: flex;
        justify-content: flex-end;
    }
    .auth--areas-action a {
        color: var(--color-primary);
        font-size: smaller;
        transition: color 0.4s linear;
    }
    .auth--areas-action a:hover {
        color: var(--color-terciary);
    }
    .auth--areas-button,
    .auth--areas-register {
        display: flex;
        justify-content: center;
    }
    .auth--areas-buttoncontent,
    .auth--areas-registeraction {
        width: 100%;
        max-width: 340px;
        display: flex;
        padding: 15px 0;
        justify-content: center;
    }
    .auth--areas-span,
    .register-action {
        font-family: Roboto, sans-serif;
        font-size: 14px;
    }

    .auth--areas-registeraction .register-action {
        color: var(--color-primary);
        cursor: pointer;
        transition: color 0.3s linear;
    }
    .auth--areas-registeraction .register-action:hover {
        color: var(--color-secundary);
    }

    .height--flex {
        height: max-content;
    }
    .auth--action-register {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
    }
    .auth--action-register .auth-areas-signin {
        color: var(--color-primary);
        font-weight: 500;
    }

    .login--content-genero {
        width: 100%;
        max-width: 340px;
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
        flex-direction: column;
        margin-bottom: 25px;
    }
    .login--genero-groups {
        display: flex;
    }
    .login--content-type {
        padding-bottom: 5px;
    }
    .login--content-type span {
        font-family: 'Roboto', sans-serif;
        color: #555;
        font-size: small;
    }
    .login--genero-options {
        padding: 10px;
        display: flex;
        align-items: center;
        border: 1px solid #d9d9d9;
        cursor: pointer;
        margin-right: 10px;
    }
    .login--genero-options label {
        width: 90px;
        font-family: 'Roboto', sans-serif;
        cursor: pointer;
        color: #555;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    .space--bottom {
        margin-bottom: 25px;
    }
    @media only screen and (max-width: 1040px) {
        width: 100%;
    }
`;
