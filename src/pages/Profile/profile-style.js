import styled from 'styled-components';

export const ProfileContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
`;

export const ProfileContent = styled.div`
    width: 80%;
    height: max-content;
    display: flex;
    justify-content: space-between;
    margin-top: 50px;

    @media only screen and (max-width: 1130px) {
        & {
            width: 90%;
        }
    }
    @media only screen and (max-width: 930px) {
        & {
            flex-direction: column;
        }
    }
`;
