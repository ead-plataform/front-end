import { Route, Switch, Redirect } from 'react-router-dom';
import ProfileAside from '../../components/ProfileAside';
import ProfileEditInfo from '../../components/ProfileEditInfo';
import ProfileEditPass from '../../components/ProfileEditPass';

import { ProfileContainer, ProfileContent } from './profile-style';

export default () => (
    <ProfileContainer>
        <ProfileContent>
            <ProfileAside />
            <Switch>
                <Route
                    path="/app/profile/me"
                    exact
                    component={ProfileEditInfo}
                />
                <Route
                    path="/app/profile/me/password"
                    exact
                    component={ProfileEditPass}
                />
                <Route>
                    <Redirect to="/app" />
                </Route>
            </Switch>
        </ProfileContent>
    </ProfileContainer>
);
