import { LogOut } from '../../helpers/auth-user';

export const DestroySession = () => LogOut();
