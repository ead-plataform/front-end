import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';

import { BannerAuth } from '../../components/BannerAuth';
import { Button } from '../../components/Button';
import Loading from '../../components/Load';
import AlertMessage from '../../components/AlertMessage';
import { SigninContainer } from '../Signin/signin-syle';

import api from '../../services/api';

const ForgotPassword = () => {
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [focused, setFocused] = useState(false);
    const [message, setMessage] = useState({});
    const [messageAlert, setMessageAlert] = useState('');
    document.title = 'Recuperar Senha';

    const handleForm = async (event) => {
        event.preventDefault();
        setLoading(true);
        if (email.length !== 0) {
            const forgot = await api.forgotPassword(
                { email },
                '/forgot-password'
            );
            if (forgot.success) {
                setLoading(false);
                setMessageAlert(forgot.success);
                setMessage({
                    severity: 'success',
                    variant: 'filled',
                    show: true,
                });
                setTimeout(() => {
                    history.push('/');
                }, 6000);
            }
            if (forgot.error) {
                setLoading(false);
                setMessageAlert(forgot.error);
                setMessage({
                    severity: 'error',
                    variant: 'filled',
                    show: true,
                });
                setTimeout(() => {
                    setMessage({ show: false });
                }, 2500);
            }
        } else {
            setLoading(false);
            setMessageAlert('Insira um email válido');
            setMessage({
                severity: 'error',
                variant: 'filled',
                show: true,
            });
            setTimeout(() => {
                setMessage({ show: false });
            }, 2500);
        }
    };

    return (
        <>
            <AlertMessage
                severity={message.severity}
                variant={message.variant}
                show={message.show}
            >
                {messageAlert}
            </AlertMessage>
            <BannerAuth />
            <SigninContainer>
                <form className="auth" method="POST" onSubmit={handleForm}>
                    <div className="auth--areas-banner">
                        <img
                            src="/assets/image/profile.svg"
                            alt="profile"
                            className="auth--areas-profile"
                        />
                        <h1 className="auth--action-span">Recuperar Conta</h1>
                    </div>
                    <div className="auth--ares-forms">
                        <div
                            className="auth--areas-content"
                            style={{
                                borderColor: focused && '#595bf9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <EmailOutlinedIcon
                                    style={{
                                        color: focused ? '#595bf9' : 'd9d9d9',
                                    }}
                                    fontSize="small"
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type="email"
                                    name="email"
                                    required
                                    placeholder="exemplo@gmail.com"
                                    className="input--info"
                                    value={email}
                                    onChange={(e) =>
                                        setEmail(e.currentTarget.value)
                                    }
                                    onFocus={() => setFocused(true)}
                                    onBlur={() => setFocused(false)}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="auth--areas-button">
                        <div className="auth--areas-buttoncontent">
                            <Button
                                color="#595bf9"
                                style={{
                                    cursor: loading ? 'not-allowed' : 'pointer',
                                }}
                            >
                                {loading ? <Loading /> : 'Recuperar Senha'}
                            </Button>
                        </div>
                    </div>
                    <div className="auth--areas-register">
                        <div className="auth--areas-registeraction">
                            <span className="auth--areas-span">
                                Deseja Voltar?
                            </span>
                            &nbsp;
                            <Link to="/" className="register-action">
                                Voltar
                            </Link>
                        </div>
                    </div>
                </form>
            </SigninContainer>
        </>
    );
};

export default ForgotPassword;
