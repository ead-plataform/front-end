import React from 'react';

import { Button } from '../../components/Button';

import {
    AreaNotFound,
    AreaNotFoundContent,
    AreaNotFoundLeft,
    AreaNotFoundRight,
    AreaNotFoundImage,
} from './pageNotFound-style';

const PageNotFound = () => {
    document.title = '404 | CérebroNerd';

    const handlePage = () => {
        window.location.href = '/';
    };

    return (
        <>
            <AreaNotFound>
                <AreaNotFoundContent>
                    <AreaNotFoundLeft>
                        <AreaNotFoundImage src="/assets/image/404.svg" />
                    </AreaNotFoundLeft>
                    <AreaNotFoundRight>
                        <h1 className="area-notfound--message">Opss!</h1>
                        <br />
                        <br />
                        <h2 className="area-notfound--status">
                            Esta Página não está disponível.
                        </h2>
                        <br />
                        <br />
                        <Button color="#595bf9" onClick={handlePage}>
                            Voltar para o início
                        </Button>
                    </AreaNotFoundRight>
                </AreaNotFoundContent>
            </AreaNotFound>
        </>
    );
};

export default PageNotFound;
