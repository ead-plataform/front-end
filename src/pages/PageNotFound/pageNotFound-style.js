import styled from 'styled-components';

export const AreaNotFound = styled.div`
    width: 100%;
    height: 100vh;
    max-width: 1000px;
    display: flex;
    align-items: center;
    position: fixed;
    left: 13vw;

    @media (max-width: 1050px) {
        left: 0;
        justify-content: center;
    }
`;

export const AreaNotFoundContent = styled.div`
    width: 100%;
    height: 50vh;
    display: flex;
    position: relative;

    @media (max-width: 1050px) {
        flex-direction: column;
        align-items: center;
        justify-content: space-around;
        max-width: 320px;
    }
`;

export const AreaNotFoundLeft = styled.div`
    width: 70%;

    @media (max-width: 1050px) {
        display: flex;
        justify-content: center;
    }
`;

export const AreaNotFoundImage = styled.img`
    width: inherit;
    @media (max-width: 1050px) {
        width: 30vh;
    }
`;

export const AreaNotFoundRight = styled.div`
    width: 30%;
    position: absolute;
    right: 130px;
    top: 20px;

    .area-notfound--message {
        font-size: calc(4vw);
        font-family: 'Roboto', sans-serif;
        color: #6c63ff;
    }
    .area-notfound--status {
        font-size: 20px;
        font-family: 'Roboto', sans-serif;
        color: #2f2e41;
    }

    @media (max-width: 1050px) {
        position: revert;
        width: 100%;
    }
`;
