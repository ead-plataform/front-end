import { useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import { SigninContainer } from '../Signin/signin-syle';
import { BannerAuth } from '../../components/BannerAuth';
import { Button } from '../../components/Button';
import AlertMessage from '../../components/AlertMessage';
import Loading from '../../components/Load';

import api from '../../services/api';

const useQueryString = () => new URLSearchParams(useLocation().search);

const ResetPassword = () => {
    const query = useQueryString();
    const history = useHistory();
    const [password, setPassword] = useState('');
    const [confirmPassword, setPasswordConfirm] = useState('');
    const [viewed, setViewed] = useState(false);
    const [viewedConfirm, setViewedConfirm] = useState(false);
    const [focusedPass, setFocusedPass] = useState(false);
    const [focusedConfirm, setFocusedConfirm] = useState(false);
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState({});
    const [messageAlert, setMessageAlert] = useState('');

    const handleForm = async (event) => {
        event.preventDefault();
        const token = query.get('token');
        setLoading(true);
        if (password === confirmPassword) {
            const resetPassword = await api.resetPassword(
                { token, password, confirmPassword },
                '/reset'
            );
            if (resetPassword.success) {
                setLoading(false);
                setMessageAlert(resetPassword.success);
                setMessage({
                    variant: 'filled',
                    severity: 'success',
                    show: true,
                });
                setTimeout(() => {
                    history.push('/');
                }, 4500);
            }
            if (resetPassword.error) {
                setLoading(false);
                setMessageAlert('token inválido');
                setMessage({
                    variant: 'filled',
                    severity: 'error',
                    show: true,
                });
                setTimeout(() => {
                    setMessage({ show: false });
                }, 2500);
            }
        } else {
            setLoading(false);
            setMessageAlert('senhas não conferem');
            setMessage({
                variant: 'filled',
                severity: 'error',
                show: true,
            });
            setTimeout(() => {
                setMessage({ show: false });
            }, 2500);
        }
    };

    const handleView = () => {
        setViewed(!viewed);
    };

    const handleViewConfirm = () => {
        setViewedConfirm(!viewedConfirm);
    };

    return (
        <>
            <AlertMessage
                variant={message.variant}
                severity={message.severity}
                show={message.show}
            >
                {messageAlert}
            </AlertMessage>
            <BannerAuth />
            <AlertMessage />
            <SigninContainer>
                <form className="auth" method="POST" onSubmit={handleForm}>
                    <div className="auth--areas-banner">
                        <img
                            src="/assets/image/profile.svg"
                            alt="profile"
                            className="auth--areas-profile"
                        />
                        <h1 className="auth--action-span">Resetar senha</h1>
                    </div>
                    <div className="auth--ares-forms">
                        <div
                            className="auth--areas-contentpass space--bottom"
                            style={{
                                borderColor: focusedPass
                                    ? '#595bf9'
                                    : '#d9d9d9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <LockOutlinedIcon
                                    fontSize="small"
                                    style={{
                                        color: focusedPass
                                            ? '#595bf9'
                                            : '#d9d9d9',
                                    }}
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type={viewed ? 'text' : 'password'}
                                    name="password"
                                    required
                                    placeholder="Sua nova senha"
                                    className="input--info"
                                    value={password}
                                    onChange={(e) =>
                                        setPassword(e.currentTarget.value)
                                    }
                                    onFocus={() => setFocusedPass(true)}
                                    onBlur={() => setFocusedPass(false)}
                                />
                            </div>
                            <div
                                className="auth--areas-view"
                                role="presentation"
                                onClick={handleView}
                            >
                                {viewed && (
                                    <VisibilityOffIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                                {!viewed && (
                                    <VisibilityOutlinedIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                            </div>
                        </div>
                        <div
                            className="auth--areas-contentpass space--bottom"
                            style={{
                                borderColor: focusedConfirm
                                    ? '#595bf9'
                                    : '#d9d9d9',
                            }}
                        >
                            <div className="auth--areas-i">
                                <LockOutlinedIcon
                                    fontSize="small"
                                    style={{
                                        color: focusedConfirm
                                            ? '#595bf9'
                                            : '#d9d9d9',
                                    }}
                                />
                            </div>
                            <div className="auth--areas-input">
                                <input
                                    type={viewedConfirm ? 'text' : 'password'}
                                    name="password"
                                    required
                                    placeholder="Confirme sua senha"
                                    className="input--info"
                                    value={confirmPassword}
                                    onChange={(e) =>
                                        setPasswordConfirm(
                                            e.currentTarget.value
                                        )
                                    }
                                    onFocus={() => setFocusedConfirm(true)}
                                    onBlur={() => setFocusedConfirm(false)}
                                />
                            </div>
                            <div
                                className="auth--areas-view"
                                role="presentation"
                                onClick={handleViewConfirm}
                            >
                                {viewedConfirm && (
                                    <VisibilityOffIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                                {!viewedConfirm && (
                                    <VisibilityOutlinedIcon
                                        fontSize="small"
                                        style={{ color: '#d9d9d9' }}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="auth--areas-button">
                        <div className="auth--areas-buttoncontent">
                            <Button
                                color="#595bf9"
                                style={{
                                    cursor: loading ? 'not-allowed' : 'pointer',
                                }}
                            >
                                {loading ? <Loading /> : 'Resetar Senha'}
                            </Button>
                        </div>
                    </div>
                </form>
            </SigninContainer>
        </>
    );
};

export default ResetPassword;
