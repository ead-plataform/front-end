import { useEffect, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { ContainerConfirmation } from '../Confirmation/style-confirmation';
import { CheckingTitleValidate } from './checking-style';
import api from '../../services/api';
import AlertMessage from '../../components/AlertMessage';

const useQueryString = () => new URLSearchParams(useLocation().search);

export default () => {
    const query = useQueryString();
    const history = useHistory();
    const [message, setMessage] = useState({});
    const [messageAlert, setMessageAlert] = useState('');

    useEffect(async () => {
        const token = query.get('token');
        if (token) {
            const checking = await api.checkingEmailSignup(
                token,
                'enable-account'
            );
            if (checking === true) {
                setMessageAlert('Faça Login na sua conta');
                setMessage({
                    variant: 'filled',
                    severity: 'success',
                    show: true,
                });
                setTimeout(() => {
                    history.push('/');
                }, 5000);
            }
            if (checking.error) {
                setMessageAlert(checking.error);
                setMessage({
                    variant: 'filled',
                    severity: 'error',
                    show: true,
                });
                setTimeout(() => {
                    history.push('/');
                }, 5000);
            }
        }
    }, []);

    return (
        <>
            <AlertMessage
                variant={message.variant}
                severity={message.severity}
                show={message.show}
            >
                {messageAlert}
            </AlertMessage>
            <ContainerConfirmation>
                <CheckingTitleValidate>
                    Validando Email...
                </CheckingTitleValidate>
            </ContainerConfirmation>
        </>
    );
};
