import { Link } from 'react-router-dom';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {
    ContainerConfirmation,
    ContainerContent,
    ContainerImage,
    ContainerTitle,
    ContainerDescription,
    ContainerNav,
} from './style-confirmation';

export default () => (
    <ContainerConfirmation>
        <ContainerContent>
            <ContainerImage>
                <img src="/assets/image/brain.svg" alt="cerebro" />
            </ContainerImage>
            <ContainerTitle>
                <h1>Confirme o seu e-mail</h1>
            </ContainerTitle>
            <ContainerDescription>
                <p>
                    Finalize seu cadastro através do link que enviamos no seu
                    e-mail para fazer parte dessa comunidade incrível de
                    estudantes.
                </p>
            </ContainerDescription>
            <ContainerNav>
                <Link to="/signin">
                    <ArrowBackIcon fontSize="medium" />
                    <span>Voltar para login</span>
                </Link>
            </ContainerNav>
        </ContainerContent>
    </ContainerConfirmation>
);
