import styled from 'styled-components';

export const ContainerConfirmation = styled.div`
    width: 100%;
    height 100vh;
    background-color: var(--color-secundary);
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const ContainerContent = styled.div`
    width: 100%;
    max-width: 600px;
`;

export const ContainerImage = styled.div`
  width: 100%
  height: 250px;
  display: flex;
  justify-content: center;

  img {
      width: 100px;
  }
`;
export const ContainerTitle = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100px;

    h1 {
        font-size: 54px;
        color: rgb(225, 225, 230);
    }

    @media only screen and (max-width: 768px) {
        & > h1 {
            font-size: 32px;
        }
    }
`;
export const ContainerDescription = styled.div`
    display: flex;
    justify-content: center;
    width: 100%;

    p {
        max-width: 80%;
        font-family: 'Roboto', sans-serif;
        text-align: center;
        color: rgb(168, 168, 179);
        line-height: 1.6;
    }
`;
export const ContainerNav = styled.div`
    width: 100%;
    height: 30px;
    display: flex;
    justify-content: center;

    a {
        display: flex;
        align-items: center;
    }
    span,
    svg {
        color: rgb(130, 87, 230);
    }
    span {
        font-size: 14px;
        margin-left: 10px;
    }
    a:hover {
        color: var(--color-primary);
    }
`;
