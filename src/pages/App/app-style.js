import styled from 'styled-components';

export const AppContainer = styled.div`
    display: flex;
    width: 100%;
    height: 100vh;
`;

export const AppContainerRight = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
`;
export const AppContainerMain = styled.main`
    width: 100%;
    height: calc(100vh - 70px);
    background: #fff;
    overflow-y: auto;

    &::-webkit-scrollbar {
        background-color: transparent;
        width: 6px;
        height: 7px;
    }
    &::-webkit-scrollbar-button {
        display: none;
    }
    &::-webkit-scrollbar-thumb {
        background-color: rgb(186, 186, 192);
        border-radius: 6px;
        border: 5px solid transparent;
    }
    &::-webkit-scrollbar-track {
        border-radius: 6px;
        background-color: rgba(0, 0, 0, 0.03);
    }
`;
