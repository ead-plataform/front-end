import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { AppProvider } from '../../contexts/AppContext';

import Header from '../../components/Header';
import Aside from '../../components/Aside';
import Course from '../../components/Course';
import Disciplines from '../../components/Disciplines';
import PageNotFound from '../PageNotFound';
import Profile from '../Profile';
import MenuMobileExited from '../../components/MenuMobileExited';
import Module from '../../components/Module';
import Classe from '../../components/Classe';
import AddCourse from '../../components/Admin/AddCourseForm';
import AddDiscipline from '../../components/Admin/AddDisciplineForm';
import AddModule from '../../components/Admin/AddModuleForm';
import DashBoard from '../../components/Dashboard';

import { LogOut } from '../../helpers/auth-user';

import { AppContainer, AppContainerRight, AppContainerMain } from './app-style';

const App = () => {
    const { url } = useRouteMatch();

    return (
        <>
            <AppProvider>
                <AppContainer>
                    <Aside />
                    <AppContainerRight>
                        <Header />
                        <MenuMobileExited />
                        <AppContainerMain>
                            <Switch>
                                <Route
                                    path={`${url}/`}
                                    exact
                                    component={DashBoard}
                                />
                                <Route
                                    path={`${url}/cursos`}
                                    exact
                                    component={Course}
                                />
                                <Route
                                    path={`${url}/cursos/:curso`}
                                    exact
                                    component={Disciplines}
                                />
                                <Route
                                    path={`${url}/cursos/:curso/:discipline`}
                                    exact
                                    component={Module}
                                />
                                <Route
                                    path={`${url}/cursos/:curso/:discipline/:aula`}
                                    exact
                                    component={Classe}
                                />
                                <Route
                                    path={`${url}/materiais`}
                                    exact
                                    component={() => <h5>Materiais</h5>}
                                />
                                <Route
                                    path={`${url}/profile/me`}
                                    component={Profile}
                                />
                                <Route
                                    path={`${url}/exit`}
                                    exact
                                    component={LogOut}
                                />
                                <Route
                                    path={`${url}/admin/course/create`}
                                    exact
                                    component={AddCourse}
                                />
                                <Route
                                    path={`${url}/admin/course/discipline/create`}
                                    exact
                                    component={AddDiscipline}
                                />
                                <Route
                                    path={`${url}/admin/course/discipline/module/create`}
                                    exact
                                    component={AddModule}
                                />
                                <Route component={PageNotFound} />
                            </Switch>
                        </AppContainerMain>
                    </AppContainerRight>
                </AppContainer>
            </AppProvider>
        </>
    );
};

export default App;
