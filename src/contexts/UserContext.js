import { createContext, useContext, useReducer, useEffect } from 'react';
import { initialStateUser, UserReducer } from '../reducers';

export const UserContext = createContext();

const localStateUser = JSON.parse(localStorage.getItem('credentails'));

export const UserProvider = ({ children }) => {
    const [state, dispatch] = useReducer(
        UserReducer,
        localStateUser || initialStateUser
    );
    useEffect(() => {
        localStorage.setItem('credentails', JSON.stringify(state));
    }, [state]);
    return (
        <UserContext.Provider value={[state, dispatch]}>
            {children}
        </UserContext.Provider>
    );
};

export const AppUserValue = () => useContext(UserContext);
