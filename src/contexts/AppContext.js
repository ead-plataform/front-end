import { createContext, useContext, useReducer, useEffect } from 'react';
import { initialState, MainReducer } from '../reducers';

export const AppContext = createContext();

const localState = JSON.parse(localStorage.getItem('menu'));

export const AppProvider = ({ children }) => {
    const [state, dispatch] = useReducer(
        MainReducer,
        localState || initialState
    );
    useEffect(() => {
        localStorage.setItem('menu', JSON.stringify(state));
    }, [state]);

    return (
        <>
            <AppContext.Provider value={[state, dispatch]}>
                {children}
            </AppContext.Provider>
        </>
    );
};

export const AppUseValue = () => useContext(AppContext);
