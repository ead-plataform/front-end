import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './styles/body/normalize.css';
import './styles/body/reset.css';
import './styles/body/app-brain.css';

import { UserProvider } from './contexts/UserContext';

ReactDOM.render(
    <UserProvider>
        <App />
    </UserProvider>,
    document.getElementById('root')
);
