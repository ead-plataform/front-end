import { MenuReducers } from './MenuReducers';
import { UserReducers } from './UserReducer';
import { CourseReducers } from './CourseReducers';
/* UserReducers for UserContext */

export const initialStateUser = {
    user: UserReducers(),
};

export const UserReducer = (state, action) => ({
    user: UserReducers(state.user, action),
});

/* end UserReducers */

export const initialState = {
    menu: MenuReducers(),
    course: CourseReducers(),
};

export const MainReducer = (state, action) => ({
    menu: MenuReducers(state.menu, action),
    course: CourseReducers(state.course, action),
});
