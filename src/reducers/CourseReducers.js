const initialState = {
    id: 'any_id',
    title: 'any_title',
    slug: 'any_slug',
    discipline: {
        id: 'any_id',
        title: 'any_title',
        slug: 'any_slug',
        qt_modules: 0,
        qt_concluded: 0,
        percentu: 'any_percentu',
    },
    moduleId: {
        id: 'any_id',
        title: '',
        qtMaterials: 0,
    },
    classes: [],
};

export const CourseReducers = (state = initialState, action = {}) => {
    switch (action.type) {
        case 'idcourse':
            return { ...state, id: action.id };
        case 'titlecourse':
            return { ...state, title: action.title };
        case 'slugcourse':
            return { ...state, slug: action.slug };
        case 'iddiscipline':
            const { discipline } = state; // eslint-disable-line
            discipline.id = action.id;
            return { ...state, discipline };
        case 'disciplinetitle':
            const { discipline: title } = state; // eslint-disable-line
            title.title = action.title;
            return { ...state, discipline: title };
        case 'slugdiscipline':
            const { discipline: disci } = state; // eslint-disable-line
            disci.slug = action.slug;
            return { ...state, discipline: disci };
        case 'discqtmodule':
            const { discipline: modules } = state; // eslint-disable-line
            modules.qt_modules = action.qt_modules;
            return { ...state, discipline: modules };
        case 'discqconclu':
            const { discipline: conclu } = state; // eslint-disable-line
            conclu.qt_concluded = action.qt_concluded;
            return { ...state, discipline: conclu };
        case 'porcetu':
            const { discipline: percen } = state; // eslint-disable-line
            percen.percentu = action.percentu;
            return { ...state, discipline: percen };
        case 'idmodule':
            const { moduleId } = state; // eslint-disable-line
            moduleId.id = action.id;
            return { ...state, moduleId };
        case 'setQtMaterials':
            const { moduleId: result } = state; // eslint-disable-line
            result.qtMaterials = action.id;
            return { ...state, moduleId: result };
        case 'setTitleModule':
            const { moduleId: params } = state; // eslint-disable-line
            params.title = action.title;
            return { ...state, moduleId: params };
        case 'setClasses':
            const { classes } = state; // eslint-disable-line
            return { ...state, classes: action.classes };
        default:
            return state;
    }
};
