const initialState = {
    menuOpened: true,
};

// eslint-disable-next-line

export const MenuReducers = (state = initialState, action = {}) => {
    switch (action.type) { //eslint-disable-line
        case 'setMenu':
            return { ...state, menuOpened: action.menuOpened };
            break; // eslint-disable-line
    }
    return state;
};
