const initialState = {
    id: '',
    email: '',
    name: '',
    phone: '',
    zipcode: '',
    address: '',
    number: '',
    complement: '',
    bairro: '',
    city: '',
    state: '',
    payment: '',
    view_free: '',
    private: '',
    profile: '',
    status: '',
    token: '',
    message: true,
};

export const UserReducers = (state = initialState, action = {}) => {
    switch (action.type) {
        case 'addid':
            return { ...state, id: action.id };
        case 'addpayment':
            return { ...state, payment: action.payment };
        case 'addviewfree':
            return { ...state, view_free: action.view_free };
        case 'addprivate':
            return { ...state, private: action.private };
        case 'addstatus':
            return { ...state, status: action.status };
        case 'addprofile':
            return { ...state, profile: action.profile };
        case 'addname':
            return { ...state, name: action.name };
        case 'addemail':
            return { ...state, email: action.email };
        case 'addcpf':
            return { ...state, cpf: action.cpf };
        case 'addphone':
            return { ...state, phone: action.phone };
        case 'addzipcode':
            return { ...state, zipcode: action.zipcode };
        case 'addaddress':
            return { ...state, address: action.address };
        case 'addnumber':
            return { ...state, number: action.number };
        case 'addcomplement':
            return { ...state, complement: action.complement };
        case 'addbairro':
            return { ...state, bairro: action.bairro };
        case 'addcity':
            return { ...state, city: action.city };
        case 'addstate':
            return { ...state, state: action.state };
        case 'addtoken':
            return { ...state, token: action.token };
        case 'setMessage':
            return { ...state, message: action.message };
        default:
            return state;
    }
    return state; // eslint-disable-line
};
