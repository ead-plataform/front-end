import { BrowserRouter, Switch, Route, HashRouter } from 'react-router-dom';
import { PrivateRoute } from './configs/priviting-router';

import Signup from './pages/Signup';
import Signin from './pages/Signin';
import ForgotPassword from './pages/ForgotPassword';
import ResetPassword from './pages/ResetPassword';
import PageNotFound from './pages/PageNotFound';
import Confirmation from './pages/Confirmation';
import CheckingEmailSignup from './pages/CheckingEmailSignup';
import { DestroySession } from './pages/DestroySession';
import App from './pages/App';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route path="/" exact component={Signin} />
            <Route path="/signin" exact component={Signin} />
            <Route path="/signup" exact component={Signup} />
            <Route path="/forgot_password" exact component={ForgotPassword} />
            <Route path="/reset_password" component={ResetPassword} />
            <Route path="/signup/confirmation" exact component={Confirmation} />
            <Route path="/verify" component={CheckingEmailSignup} />
            <PrivateRoute path="/app" component={App} />
            <PrivateRoute path="/app/exit" component={DestroySession} />
            <Route component={PageNotFound} />
        </Switch>
    </BrowserRouter>
);

export default Routes;
