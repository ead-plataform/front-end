import styled from 'styled-components';

export const ModuleContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;

    .asign--navigation-redirect {
        width: 50%;
        display: flex;
        align-items: center;
    }

    @media only screen and (max-width: 975px) {
        & {
            padding: 10px;
        }
    }
    @media only screen and (max-width: 600px) {
        .asign--navigation-redirect {
            width: 100%;
        }
    }
`;

export const ModuleProgress = styled.div`
    width: 50%;
    max-width: 450px;
    height: inherit;
    display: flex;
    flex-direction: column;
    justify-content: center;

    .progress--info {
        display: flex;
        justify-content: space-between;
    }
    .progress--info > span {
        font-size: small;
        font-family: 'Roboto', sans-serif;
    }
    .progress--status {
        width: 100%;
        height: 8px;
        border-radius: 5px;
        background: #d8dde7;
    }

    .progress--percentu {
        width: 36%;
        height: inherit;
        background-color: var(--color-primary);
        border-radius: 5px;
    }
    @media only screen and (max-width: 600px) {
        & {
            width: 100%;
        }
    }
`;

export const ModuleContent = styled.div`
    flex: 1;
    width: 100%;
    max-width: 1120px;
    padding: 0 0.6em;
    display: flex;
    justify-content: center;
`;

export const ModuleListModule = styled.div`
    flex-basis: 50%;
    height: calc(100vh - 170px);
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    overflow-y: auto;

    scrollbar-width: none;

    &::-webkit-scrollbar {
        display: none;
    }

    @media only screen and (max-width: 765px) {
        & {
            flex: 1;
            height: max-content;
        }
    }
`;

export const ModuleContentCard = styled.div`
    width: 100%;
    max-width: 520px;
    height: max-content;
    display: flex;
    flex-direction: column;
    background-size: 2px 8px;
    background-image: linear-gradient(
        rgb(206, 208, 221) 50%,
        rgb(255, 255, 255) 50%
    );
    background-repeat: repeat-y;
    background-position-x: 7px;
    position: relative;
    margin-top: 25px;
    margin-bottom: 60vh;

    &:first-child {
        margin-top: 0px;
    }

    @media only screen and (max-width: 845px) {
        & {
            margin-right: 20px;
        }
    }
    @media only screen and (max-width: 765px) {
        & {
            overflow: hidden;
            height: max-content;
            padding-bottom: 30px;
            max-width: none;
            margin-right: 0px;
            margin-bottom: 0px;
        }
    }
`;

export const ModuleCard = styled.button`
    text-align: left;
    border: none;
    outline: none;
    background: transparent;
    position: relative;
    width: 100%;
    display: flex;
    cursor: pointer;
    padding-left: 0px;
    padding-top: 20px;
    padding-bottom: unset;
    user-select: none;

    &:first-child::before {
        content: ' ';
        width: 16px;
        height: 40px;
        background: #fff;
        position: absolute;
        top: -10px;
        left: 0px;
        z-index: 1;
    }
    &:last-child::before {
        content: ' ';
        width: 16px;
        height: 100%;
        background: #fff;
        position: absolute;
        top: 24px;
        left: 0px;
        z-index: 1;
    }
`;

export const ModuleCardBobble = styled.span`
    width: 16px;
    height: 16px;
    background: rgb(240, 241, 247);
    border-radius: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 8px;
    margin-top: 8px;
    z-index: 2;

    &:before {
        display: block;
        content: ' ';
        background: ${(props) =>
            props.percentu > 0 ? '#595bf9' : 'rgb(206, 208, 221)'};
        border-radius: 100%;
        width: 8px;
        height: 8px;
    }
`;

export const ModuleCardInfo = styled.div`
    display: flex;
    flex: 1 1 0%;
    background: rgb(255, 255, 255);
    border-radius: 4px;
    box-shadow: rgb(0 0 0 / 10%) 0px 1px 2px;
    padding: 16px;
    flex-direction: column;
    position: relative;
    -webkit-box-pack: center;
    justify-content: center;
    margin-right: 8px;

    ${ModuleCard}.focused & {
        box-shadow: rgb(0 0 0 / 10%) 0px 6px 12px;
    }

    ${ModuleCard}.focused &::before {
        position: absolute;
        right: -8px;
        content: ' ';
        width: 16px;
        height: 16px;
        background: rgb(255, 255, 255);
        transform: rotate(45deg);
        box-shadow: rgb(0 0 0 / 5%) 5px -4px 6px 0px;
    }
`;

export const ModuleCardTitle = styled.p`
    display: flex;
    align-items: center;
    margin-top: 0px;
    margin-bottom: 16px;

    ${ModuleCard}.focused &:nth-child(2) {
        display: none;
    }

    strong {
        flex: 1 1 0%;
        font-size: 15px;
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        color: rgb(156, 159, 176);
    }

    ${ModuleCard}.focused & .arrow-focus {
        display: none;
    }
`;

export const ModuleCardProgre = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    min-height: 19px;
`;

export const ModuleCardProgreBar = styled.div`
    flex: 1 1 auto;
    margin-right: 16px;
    height: 8px;
    border-radius: 4px;
    background: #d8dde7;
`;

export const ModuleCardProgreStatus = styled.div`
    height: inherit;
    border-radius: 4px;
    background: var(--color-primary);
`;

export const ModuleCardSpan = styled.p`
    margin: 0px;
    font-size: 14px;
    color: rgb(37, 39, 45);
    font-family: 'Nunito Sans', sans-serif;
`;

export const ModuleListDetails = styled.div`
    flex-basis: 50%;
    margin-top: 20px;
    height: 390px;
    display: flex;
    justify-content: flex-end;
    transition: display 0.3s linear;

    @media only screen and (max-width: 765px) {
        & {
            display: none;
            width: 100%;
            position: fixed;
            top: 0px;
            margin-top: 0px;
            height: 100vh;
            z-index: 10;
        }
    }
`;
export const ModuleListContent = styled.div`
    width: 100%;
    max-width: 520px;
    min-height: 390px;
    background: #fff;
    border-radius: 4px;
    box-shadow: rgb(0 0 0 / 10%) 0px 1px 2px;
    overflow-y: auto;

    &::-webkit-scrollbar {
        background-color: transparent;
        width: 6px;
        height: 7px;
    }
    &::-webkit-scrollbar-button {
        display: none;
    }
    &::-webkit-scrollbar-thumb {
        background-color: rgb(186, 186, 192);
        border-radius: 6px;
        border: 5px solid transparent;
    }
    &::-webkit-scrollbar-track {
        border-radius: 6px;
        background-color: rgba(0, 0, 0, 0.03);
    }

    @media only screen and (max-width: 930px) {
        & {
            max-width: 400px;
        }
    }
    @media only screen and (max-width: 765px) {
        & {
            width: 100%;
            height: 100%;
            max-width: none;
        }
    }
`;
export const ModuleListHeader = styled.header`
    width: 100%;
    min-height: 84px;
    padding: 20px 16px;
    display: flex;
    flex-direction: column;

    .header--progress-mobile {
        display: none;
        margin-top: 20px;
        flex-direction: column;
        align-items: flex-start;
    }
    .header--progress-mobile > p {
        font-weight: 600;
        margin-bottom: 10px;
    }
    .header--progress-mobile > div {
        width: 100%;
    }

    @media only screen and (max-width: 765px) {
        & {
            margin-top: 40px;
        }
        .header--progress-mobile {
            display: block;
        }
    }
`;
export const ModuleListStrong = styled.strong`
    font-family: 'Nunito', sans-serif;
    font-size: 12px;
    font-weight: bold;
    color: rgb(37, 39, 45);
    margin-bottom: 8px;
`;

export const ModuleListClose = styled.div`
    display: none;
    width: max-content;
    position: absolute;
    top: 20px;
    right: 20px;

    @media only screen and (max-width: 765px) {
        & {
            display: block;
        }
        .skeletonMaterials {
            width: 100%;
            height: 100vh;
            background-color: #fff;
        }
    }
`;
export const ModuleActiveTitle = styled.h2`
    font-family: 'Nunito', sans-serif;
    font-size: 16px;
    color: rgb(37, 39, 45);
`;
export const ModuleListBody = styled.div`
    padding: 0 10px;
    height: max-content;

    .not-before::before {
        content: none;
    }
    .list--of-class li {
        padding: 8px;
        display: flex;
        align-items: center;
        cursor: pointer;
    }
    .list--of-class .type--add-class {
        border: 2px dashed var(--color-primary);
        cursor: pointer;
        user-select: none;
        font-size: small;
    }
    .list--of-class li:hover {
        background: rgb(250, 250, 252);
    }
    .legend-class {
        font-family: 'Nunito', sans-serif;
        font-weight: 600;
        font-size: 14px;
        color: rgb(58, 62, 71);
    }
`;
export const ModuleAddButton = styled.button`
    position: relative;
    display: flex;
    align-items: center;
    height: 39.89px;
    background: #fff;
    border-top-left-radius: 6px;
    border-bottom-left-radius: 6px;
    border: 1px dashed var(--color-primary);
    cursor: pointer;
    border-right: 0;
    outline: none;
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
    color: #595bf9;

    &:before {
        position: absolute;
        top: 0;
        right: -20px;
        height: 0;
        border-top: 20px solid transparent;
        border-bottom: 20px solid transparent;
        border-left: 20px solid #fff;
        content: '';
    }
    &:after {
        position: absolute;
        top: 6px;
        right: -15px;
        width: 27px;
        height: 26px;
        border-top: 1px dashed var(--color-primary);
        border-right: 1px dashed var(--color-primary);
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        content: '';
    }
`;
