import { useState, useEffect, useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CloseIcon from '@material-ui/icons/Close';
import Brightness1Icon from '@material-ui/icons/Brightness1';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import Skeleton from '@material-ui/lab/Skeleton';
import Modal from '../Modal';
import ClassAdd from '../Admin/AddClassForm';
import MaterialAdd from '../Admin/AddMaterialForm';

import { AppUseValue } from '../../contexts/AppContext';
import { unLockAdmin } from '../../helpers/auth-unlock-admin';
import {
    ModuleContainer,
    ModuleProgress,
    ModuleContent,
    ModuleListModule,
    ModuleListDetails,
    ModuleContentCard,
    ModuleCard,
    ModuleCardBobble,
    ModuleCardInfo,
    ModuleCardTitle,
    ModuleCardProgre,
    ModuleCardProgreBar,
    ModuleCardSpan,
    ModuleCardProgreStatus,
    ModuleListContent,
    ModuleListHeader,
    ModuleListStrong,
    ModuleActiveTitle,
    ModuleListBody,
    ModuleListClose,
    ModuleAddButton,
} from './module-style';
import NavigationMenu from '../NavigationMenu';
import api from '../../services/api';
import { LoginMultipleBlocked } from '../../helpers/auth-locked-multiple-access';

const useStyles = makeStyles(() => ({
    root: {
        width: '100%',
        boxShadow: 'none',
    },
    typograph: {
        fontFamily: ' Roboto, sans-serif',
        color: 'rgb(58, 62, 71);',
        fontSize: '13px',
        fontWeight: '500',
    },
}));

export default () => {
    const isAdmin = unLockAdmin();
    const classes = useStyles();

    const history = useHistory();

    const contentCard = useRef();
    const [state, dispatch] = AppUseValue();
    const [loading, setLoading] = useState(false);
    const [modules, setModules] = useState(); // eslint-disable-line
    const [qtModules, setQtmodule] = useState(0);
    const [qtConcluded, setQtConcluded] = useState(0);
    const [percentu, setPercentu] = useState(0);
    const [loadingProgress, setLoadingProgress] = useState(false);
    const [moduleTitle, setModuleTitle] = useState('');
    const [classe, setClasse] = useState([]);
    const [materials, setMaterias] = useState([]);
    const [loadingClass, setLoadingClass] = useState(false);
    const [moduleId, setModuleId] = useState();

    const [expanded, setExpanded] = useState(false);
    const [modalOpen, setModal] = useState(false);
    const [modalOpenMaterial, setModalMaterial] = useState(false);

    document.title = 'Modulos';

    const handleViewed = async (event) => {
        await api.addHistoric('/historic', {
            id_class: event.currentTarget.dataset.id,
            moduleId: state.course.moduleId.id,
        });
        await api.setProgressModule('/progress', {
            totalItems: state.course.moduleId.qtMaterials,
            completedItems: '1',
            disciplineId: state.course.discipline.id,
            moduleId: state.course.moduleId.id,
        });
    };

    const catchMaterialsAndClass = async (id) => {
        const classesAndMaterials = await api.catchClassAndMaterials(
            'module',
            id
        );
        if (classesAndMaterials[0].materials) {
            setMaterias(classesAndMaterials[0].materials);
        } else {
            setMaterias([]);
        }
        dispatch({ type: 'idmodule', id: classesAndMaterials[0].id });
        setModuleTitle(classesAndMaterials[0].title);
        dispatch({
            type: 'setTitleModule',
            title: classesAndMaterials[0].title,
        });
        setClasse(classesAndMaterials[0].classes);
        setModuleId(classesAndMaterials[0].id);
        setLoadingClass(true);
    };

    const catchingModules = async () => {
        const moduling = await api.catchModule(
            'module',
            state.course.discipline.id
        );
        if (moduling.length > 0) {
            dispatch({ type: 'setQtMaterials', id: moduling[0].qtmaterials });
            setModules(moduling);
            setLoading(true);
            catchMaterialsAndClass(moduling[0].id);
        }
    };

    const catchProgressModules = async () => {
        const progress = await api.catchProgressModule(
            'progress',
            state.course.discipline.id
        );
        setQtmodule(progress.qt_modules);
        setQtConcluded(progress.qt_concluded);
        setPercentu(progress.percentu);
        setLoadingProgress(true);
    };

    useEffect(() => {
        LoginMultipleBlocked();
        catchProgressModules();
        catchingModules();
    }, []);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    const removeFocused = () => {
        const listOfCard = document.querySelector('.focused');
        listOfCard.classList.remove('focused');
    };

    const scrollingCard = (event) => {
        removeFocused();
        if (window.innerWidth <= 765)
            contentCard.current.style = 'display:block';

        if (window.innerWidth > 760)
            event.currentTarget.scrollIntoView({ behavior: 'smooth' });

        if (moduleId !== event.currentTarget.dataset.id) {
            setLoadingClass(false);
            dispatch({ type: 'idmodule', id: event.currentTarget.dataset.id });
            dispatch({
                type: 'setQtMaterials',
                id: event.currentTarget.dataset.materials,
            });

            catchMaterialsAndClass(event.currentTarget.dataset.id);
        }
        event.currentTarget.classList.add('focused');
    };

    const exitedContentModule = () => {
        contentCard.current.style = 'display: none';
    };

    const handleAddModule = () => {
        history.push('/app/admin/course/discipline/module/create');
    };

    const handleAddClass = () => {
        setModal(!modalOpen);
    };

    const handleAddMaterial = () => {
        setModalMaterial(!modalOpenMaterial);
    };

    return (
        <ModuleContainer>
            <Modal open={modalOpen} handleClose={handleAddClass}>
                <ClassAdd />
            </Modal>
            <Modal open={modalOpenMaterial} handleClose={handleAddMaterial}>
                <MaterialAdd />
            </Modal>
            <NavigationMenu style={{ maxWidth: '1120px', padding: '0 0.6em' }}>
                <div className="asign--navigation-redirect">
                    <Link
                        className="header--nav-link"
                        to="/app/cursos?page=1&search="
                    >
                        Cursos:
                    </Link>
                    <KeyboardArrowRightIcon fontSize="small" />
                    <Link
                        className="header--nav-link"
                        to={`/app/cursos/${state.course.slug}`}
                    >
                        {state.course.title}
                    </Link>
                    <KeyboardArrowRightIcon fontSize="small" />
                    <Link
                        className="header--nav-link active"
                        to={`/app/cursos/${state.course.slug}/${state.course.discipline.slug}`}
                    >
                        {state.course.discipline.title}
                    </Link>
                </div>
                <ModuleProgress>
                    <div className="progress--info">
                        {loadingProgress && (
                            <span>
                                {qtConcluded}/{qtModules} modúlos concluido
                            </span>
                        )}
                        <span>{percentu?.toFixed(2)}%</span>
                    </div>
                    <div className="progress--status">
                        <div
                            className="progress--percentu"
                            style={{
                                width: `${percentu}%`,
                            }}
                        />
                    </div>
                </ModuleProgress>
            </NavigationMenu>
            <ModuleContent>
                <ModuleListModule>
                    {isAdmin && (
                        <div>
                            <ModuleAddButton onClick={handleAddModule}>
                                <AddIcon style={{ color: '#595BF9' }} />
                                Adicionar módulo
                            </ModuleAddButton>
                        </div>
                    )}
                    <ModuleContentCard>
                        {loading &&
                            modules.map((item, key) => (
                                <ModuleCard
                                    onClick={scrollingCard}
                                    data-id={item.id}
                                    data-materials={item.qtmaterials}
                                    className={key === 0 && 'focused'}
                                    key={item.id}
                                >
                                    <ModuleCardBobble
                                        style={{
                                            backgroundColor:
                                                item.progress.percentu === 0
                                                    ? 'rgb(240,241,247)'
                                                    : 'rgba(89, 91, 249, 0.4)',
                                        }}
                                        percentu={item.progress.percentu}
                                    />
                                    <ModuleCardInfo>
                                        <ModuleCardTitle>
                                            <strong>{item.title}</strong>
                                            <KeyboardArrowRightIcon
                                                fontSize="small"
                                                className="arrow-focus"
                                            />
                                        </ModuleCardTitle>
                                        <ModuleCardProgre>
                                            <ModuleCardProgreBar>
                                                <ModuleCardProgreStatus
                                                    style={{
                                                        width: `${item.progress.percentu}%`,
                                                    }}
                                                />
                                            </ModuleCardProgreBar>
                                            <ModuleCardSpan>
                                                {item.progress.percentu}%
                                                Concluido
                                            </ModuleCardSpan>
                                        </ModuleCardProgre>
                                    </ModuleCardInfo>
                                </ModuleCard>
                            ))}
                        {!loading && (
                            <>
                                <ModuleCard>
                                    <ModuleCardBobble />
                                    <ModuleCardInfo>
                                        <Skeleton
                                            variant="rect"
                                            style={{
                                                width: '90%',
                                                height: '25px',
                                                marginBottom: '10px',
                                            }}
                                        />
                                        <ModuleCardProgre>
                                            <ModuleCardProgreBar>
                                                <Skeleton
                                                    style={{
                                                        width: '34%',
                                                        height: '8px',
                                                    }}
                                                />
                                            </ModuleCardProgreBar>
                                            <ModuleCardSpan
                                                style={{ display: 'flex' }}
                                            >
                                                <Skeleton
                                                    variant="rect"
                                                    width={25}
                                                    height={20}
                                                    style={{
                                                        marginRight: '7px',
                                                    }}
                                                />
                                                <Skeleton
                                                    variant="rect"
                                                    width={40}
                                                    height={20}
                                                />
                                            </ModuleCardSpan>
                                        </ModuleCardProgre>
                                    </ModuleCardInfo>
                                </ModuleCard>
                                <ModuleCard>
                                    <ModuleCardBobble />
                                    <ModuleCardInfo>
                                        <Skeleton
                                            variant="rect"
                                            style={{
                                                width: '90%',
                                                height: '25px',
                                                marginBottom: '10px',
                                            }}
                                        />
                                        <ModuleCardProgre>
                                            <ModuleCardProgreBar>
                                                <Skeleton
                                                    style={{
                                                        width: '34%',
                                                        height: '8px',
                                                    }}
                                                />
                                            </ModuleCardProgreBar>
                                            <ModuleCardSpan
                                                style={{ display: 'flex' }}
                                            >
                                                <Skeleton
                                                    variant="rect"
                                                    width={25}
                                                    height={20}
                                                    style={{
                                                        marginRight: '7px',
                                                    }}
                                                />
                                                <Skeleton
                                                    variant="rect"
                                                    width={40}
                                                    height={20}
                                                />
                                            </ModuleCardSpan>
                                        </ModuleCardProgre>
                                    </ModuleCardInfo>
                                </ModuleCard>
                                <ModuleCard>
                                    <ModuleCardBobble />
                                    <ModuleCardInfo>
                                        <Skeleton
                                            variant="rect"
                                            style={{
                                                width: '90%',
                                                height: '25px',
                                                marginBottom: '10px',
                                            }}
                                        />
                                        <ModuleCardProgre>
                                            <ModuleCardProgreBar>
                                                <Skeleton
                                                    style={{
                                                        width: '34%',
                                                        height: '8px',
                                                    }}
                                                />
                                            </ModuleCardProgreBar>
                                            <ModuleCardSpan
                                                style={{ display: 'flex' }}
                                            >
                                                <Skeleton
                                                    variant="rect"
                                                    width={25}
                                                    height={20}
                                                    style={{
                                                        marginRight: '7px',
                                                    }}
                                                />
                                                <Skeleton
                                                    variant="rect"
                                                    width={40}
                                                    height={20}
                                                />
                                            </ModuleCardSpan>
                                        </ModuleCardProgre>
                                    </ModuleCardInfo>
                                </ModuleCard>
                                <ModuleCard>
                                    <ModuleCardBobble />
                                    <ModuleCardInfo>
                                        <Skeleton
                                            variant="rect"
                                            style={{
                                                width: '90%',
                                                height: '25px',
                                                marginBottom: '10px',
                                            }}
                                        />
                                        <ModuleCardProgre>
                                            <ModuleCardProgreBar>
                                                <Skeleton
                                                    style={{
                                                        width: '34%',
                                                        height: '8px',
                                                    }}
                                                />
                                            </ModuleCardProgreBar>
                                            <ModuleCardSpan
                                                style={{ display: 'flex' }}
                                            >
                                                <Skeleton
                                                    variant="rect"
                                                    width={25}
                                                    height={20}
                                                    style={{
                                                        marginRight: '7px',
                                                    }}
                                                />
                                                <Skeleton
                                                    variant="rect"
                                                    width={40}
                                                    height={20}
                                                />
                                            </ModuleCardSpan>
                                        </ModuleCardProgre>
                                    </ModuleCardInfo>
                                </ModuleCard>
                            </>
                        )}
                    </ModuleContentCard>
                </ModuleListModule>
                <ModuleListDetails ref={contentCard}>
                    {loadingClass && (
                        <ModuleListContent>
                            <ModuleListHeader>
                                <ModuleListStrong>
                                    Conteúdo do módulo
                                </ModuleListStrong>
                                <ModuleActiveTitle>
                                    {loadingClass ? (
                                        moduleTitle
                                    ) : (
                                        <Skeleton
                                            variant="rect"
                                            style={{
                                                width: '60%',
                                                marginBottom: '15px',
                                            }}
                                            height={30}
                                        />
                                    )}
                                </ModuleActiveTitle>
                                <ModuleCardProgre className="header--progress-mobile">
                                    <ModuleCardSpan>
                                        0% Concluido
                                    </ModuleCardSpan>
                                    <ModuleCardProgreBar>
                                        <ModuleCardProgreStatus
                                            style={{ width: '0' }}
                                        />
                                    </ModuleCardProgreBar>
                                </ModuleCardProgre>
                                <ModuleListClose onClick={exitedContentModule}>
                                    <CloseIcon fontSize="small" />
                                </ModuleListClose>
                            </ModuleListHeader>
                            <ModuleListBody>
                                <div>
                                    <Accordion
                                        className={classes.root}
                                        style={{
                                            borderBottom:
                                                '1px solid rgb(240, 241, 247)',
                                        }}
                                        expanded={expanded === 'panel1'}
                                        onChange={handleChange('panel1')}
                                    >
                                        <AccordionSummary
                                            expandIcon={
                                                <ExpandMoreIcon
                                                    style={{
                                                        width: '0.7em',
                                                        height: '0.7em',
                                                    }}
                                                />
                                            }
                                            aria-controls="panel2a-content"
                                            id="panel2a-header"
                                        >
                                            <svg
                                                version="1.1"
                                                id="Capa_1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                x="0px"
                                                y="0px"
                                                viewBox="0 0 397.2 397.2"
                                                style={{
                                                    width: '18px',
                                                    height: '18px',
                                                    marginRight: '20px',
                                                }}
                                            >
                                                <g>
                                                    <g>
                                                        <path
                                                            d="M284.2,178l-58-33.6l-57.6-33.2c-5.6-3.2-12-4-17.6-2.4c-5.6,1.6-10.8,5.2-14,10.8
			c-1.2,1.6-1.6,3.6-2.4,5.6c-0.4,1.2-0.4,2.8-0.8,4.4c0,0.4,0,1.2,0,1.6v68v68c0,6.4,2.8,12.4,6.8,16.4c4.4,4.4,10,6.8,16.4,6.8
			c3.6,0,11.2-3.2,13.2-4.4l56.8-32.8h0.4l0.4-0.4l58.8-34c5.6-3.2,9.2-8.4,10.8-14.4c0.4-1.2,0.4-2.8,0.4-4.4
			C297.8,186.8,284.2,178,284.2,178z M276.2,201.6l-58,33.6c-0.4,0-0.8,0.4-0.8,0.4l-56.8,32.8c-0.4,0.4-2.4,1.2-3.2,1.2
			s-1.6-0.4-2.4-0.8c-0.4-0.4-0.8-1.6-0.8-2.4v-67.6v-67.6v-0.4c0-0.4,0-0.4,0-0.8c0,0,0-0.4,0.4-0.4c0,0,0.4-0.4,0.4-0.8
			c0.4-0.4,1.2-0.8,1.6-1.2c0.8,0,1.6,0,2,0c0.4,0.4,0.8,0.4,1.2,0.8l56.8,32.8c0.4,0.4,0.8,0.4,0.8,0.4h0.4l58,33.6
			c0.8,0.4,2.4,1.6,2.4,2.8C278.2,199.6,277,201.2,276.2,201.6z"
                                                        />
                                                        <path
                                                            d="M339,58.4C300.6,19.6,249.8,0,199,0S97.4,19.2,58.6,58C19.8,97.2,0.2,148,0.2,198.8
			s19.2,101.6,58,140.4s89.6,58,140.4,58c50.8,0,101.6-19.2,140.4-58c38.8-38.8,58-89.6,58-140.4S377.8,97.2,339,58.4z M325,324.8
			c-34.8,34.8-80.4,52.4-126,52.4c-45.6,0-91.2-17.6-126-52.4c-35.2-34.8-52.4-80.4-52.4-126c0-45.6,17.6-91.2,52.4-126
			s80.4-52.4,126-52.4c45.6,0,91.2,17.6,126,52.4s52.4,80.4,52.4,126C377.4,244.4,360.2,290,325,324.8z"
                                                        />
                                                    </g>
                                                </g>
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                                <g />
                                            </svg>
                                            <Typography
                                                className={classes.typograph}
                                                component="span"
                                            >
                                                Assistir Vídeos Relacionados
                                            </Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Typography
                                                style={{ width: '100%' }}
                                                component="span"
                                            >
                                                <ul className="list--of-class">
                                                    {isAdmin && (
                                                        <li
                                                            className="type--add-class"
                                                            onClick={
                                                                handleAddClass
                                                            }
                                                            aria-hidden="true"
                                                        >
                                                            <AddIcon
                                                                fontSize="small"
                                                                style={{
                                                                    color:
                                                                        '#595BF9',
                                                                }}
                                                            />
                                                            Adicionar Aula
                                                        </li>
                                                    )}
                                                    {loadingClass &&
                                                        classe &&
                                                        classe.map((item) => (
                                                            <a
                                                                href={`${window.location.href}/${item.slug}`}
                                                                key={item.id}
                                                            >
                                                                <ul>
                                                                    <li>
                                                                        {item.viewed ? (
                                                                            <Brightness1Icon
                                                                                style={{
                                                                                    fontSize:
                                                                                        '12px',
                                                                                    marginRight:
                                                                                        '10px',
                                                                                    fill:
                                                                                        '#50C79D',
                                                                                }}
                                                                            />
                                                                        ) : (
                                                                            <RadioButtonUncheckedIcon
                                                                                style={{
                                                                                    fontSize:
                                                                                        '12px',
                                                                                    marginRight:
                                                                                        '10px',
                                                                                    fill:
                                                                                        '#d8dde7',
                                                                                }}
                                                                            />
                                                                        )}
                                                                        <strong className="legend-class">
                                                                            {
                                                                                item.title
                                                                            }
                                                                        </strong>
                                                                    </li>
                                                                </ul>
                                                            </a>
                                                        ))}
                                                </ul>
                                            </Typography>
                                        </AccordionDetails>
                                    </Accordion>
                                    <Accordion
                                        className={`${classes.root} not-before`}
                                        style={{
                                            borderBottom:
                                                '1px solid rgb(240, 241, 247)',
                                        }}
                                        expanded={expanded === 'pane12'}
                                        onChange={handleChange('pane12')}
                                    >
                                        <AccordionSummary
                                            expandIcon={
                                                <ExpandMoreIcon
                                                    style={{
                                                        width: '0.7em',
                                                        height: '0.7em',
                                                    }}
                                                />
                                            }
                                            aria-controls="panel2a-content"
                                            id="panel2a-header"
                                        >
                                            <svg
                                                height="18px"
                                                viewBox="-45 0 511 512"
                                                width="18px"
                                                xmlns="http://www.w3.org/2000/svg"
                                                style={{ marginRight: '20px' }}
                                            >
                                                <path d="m145.53125 177.191406h73.839844c4.175781 0 7.5625-3.382812 7.5625-7.558594 0-4.171874-3.386719-7.554687-7.5625-7.554687h-73.839844c-2.097656 0-3.871094-1.785156-3.871094-3.894531v-65.378906c0-2.109376 1.773438-3.894532 3.871094-3.894532h176.46875c2.097656 0 3.871094 1.785156 3.871094 3.894532v65.378906c0 2.113281-1.773438 3.894531-3.871094 3.894531h-67.964844c-4.179687 0-7.566406 3.382813-7.566406 7.558594 0 4.171875 3.386719 7.554687 7.566406 7.554687h67.964844c10.476562 0 18.996094-8.527344 18.996094-19.003906v-65.382812c0-10.480469-8.519532-19.007813-18.996094-19.007813h-176.46875c-10.476562 0-18.996094 8.527344-18.996094 19.007813v65.378906c0 10.480468 8.519532 19.007812 18.996094 19.007812zm0 0" />
                                                <path d="m408.175781 13.382812c-8.628906-8.628906-20.070312-13.382812-32.230469-13.382812l-299.40625.394531c-2.171874 0-4.367187-.019531-6.578124-.035156-15.113282-.136719-30.753907-.265625-43.75 5.320313-15.675782 6.734374-24.320313 20.75-25.695313 41.652343-.011719.164063-.015625.332031-.015625.496094v393.558594c0 .113281.003906.226562.007812.339843.828126 18.460938 14.988282 38.335938 44.121094 38.335938h142.179688l10.222656 13.125c.039062.050781.082031.101562.121094.152344 9.792968 11.914062 23.222656 18.660156 37.042968 18.660156.460938 0 .925782-.007812 1.390626-.023438 13.390624-.4375 25.847656-7.324218 34.207031-18.902343l10.132812-13.011719h96.394531c24.851563 0 45.066407-20.414062 45.066407-45.503906v-65.804688l.113281-323.238281c.019531-12.105469-4.714844-23.519531-13.324219-32.132813zm-339.195312 2.082032v197.046875c0 4.175781 3.382812 7.558593 7.5625 7.558593 4.179687 0 7.5625-3.382812 7.5625-7.558593v-197.011719l291.847656-.386719c8.109375 0 15.75 3.179688 21.519531 8.949219 5.75 5.757812 8.914063 13.367188 8.902344 21.4375l-.113281 323.238281c-.023438 16.765625-13.683594 30.402344-30.449219 30.402344h-41.441406c.8125-6.601563-.019532-13.003906-2.613282-18.652344-3.234374-7.039062-12.316406-19.039062-36.8125-20.25-.125-.007812-.25-.011719-.375-.011719h-8.265624v-73.039062c0-10.167969-8.671876-18.441406-19.335938-18.441406h-67.199219c-10.664062 0-19.335937 8.273437-19.335937 18.441406v73.039062h-8.296875c-.125 0-.25.003907-.375.011719-24.484375 1.214844-33.558594 13.203125-36.792969 20.238281-2.59375 5.652344-3.425781 12.058594-2.613281 18.664063h-48.25v-152.027344c0-4.171875-3.386719-7.554687-7.5625-7.554687-4.179688 0-7.566407 3.382812-7.566407 7.554687v152.027344h-24.347656c-12.296875 0-21.914062 3.605469-29 9.171875v-360.230469c2.148438-31.175781 21.679688-32.871093 53.351563-32.617187zm106.042969 449.480468h-130.394532c-20.519531 0-28.4375-12.75-29-23.742187v-2.617187c.609375-12.136719 9.792969-24.332032 29-24.332032h92.398438c1.660156 3.351563 3.703125 6.648438 6.164062 9.808594l31.847656 40.886719zm82.742187 18.957032c-.0625.078125-.121094.160156-.179687.238281-5.679688 7.921875-13.667969 12.441406-22.496094 12.730469-9.539063.296875-19.078125-4.441406-26.183594-13.050782l-53.773438-69.039062c-2.570312-3.300781-4.535156-6.789062-5.875-10.25-.101562-.332031-.226562-.652344-.367187-.964844-2.164063-6.128906-2.316406-12.117187-.171875-16.785156 3.960938-8.625 14.984375-10.984375 23.609375-11.4375h15.667969c4.175781 0 7.5625-3.382812 7.5625-7.558594v-80.597656c0-1.773438 1.96875-3.328125 4.210937-3.328125h67.199219c2.242188 0 4.210938 1.554687 4.210938 3.328125v80.597656c0 4.175782 3.382812 7.558594 7.5625 7.558594h15.636718c8.632813.453125 19.664063 2.816406 23.628906 11.449219 2.144532 4.675781 1.996094 10.667969-.171874 16.796875-.136719.296875-.253907.601562-.351563.917968-1.339844 3.46875-3.304687 6.964844-5.878906 10.269532zm118.558594-18.957032h-84.613281l-.011719.003907 31.847656-40.886719c2.460937-3.160156 4.503906-6.457031 6.160156-9.808594h46.105469c11.691406 0 22.371094-4.429687 30.449219-11.699218v32.003906c0 16.753906-13.429688 30.386718-29.9375 30.386718zm0 0" />
                                            </svg>
                                            <Typography
                                                className={classes.typograph}
                                                component="span"
                                            >
                                                Baixar Material Complementar
                                            </Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Typography
                                                style={{ width: '100%' }}
                                                component="span"
                                            >
                                                <ul className="list--of-class">
                                                    {isAdmin && (
                                                        <li
                                                            className="type--add-class"
                                                            onClick={
                                                                handleAddMaterial
                                                            }
                                                            aria-hidden="true"
                                                        >
                                                            <AddIcon
                                                                fontSize="small"
                                                                style={{
                                                                    color:
                                                                        '#595BF9',
                                                                }}
                                                            />
                                                            Adicionar Máterial
                                                        </li>
                                                    )}
                                                    {loadingClass &&
                                                        materials.map(
                                                            (item) => (
                                                                <a
                                                                    href={`${item.url}`}
                                                                    target="_blanck"
                                                                    onClick={
                                                                        handleViewed
                                                                    }
                                                                    data-id={
                                                                        item.id
                                                                    }
                                                                    key={
                                                                        item.id
                                                                    }
                                                                >
                                                                    <ul>
                                                                        <li>
                                                                            {item.viewed ? (
                                                                                <Brightness1Icon
                                                                                    style={{
                                                                                        fontSize:
                                                                                            '10px',
                                                                                        marginRight:
                                                                                            '12px',
                                                                                        fill:
                                                                                            '#50C79D',
                                                                                    }}
                                                                                />
                                                                            ) : (
                                                                                <RadioButtonUncheckedIcon
                                                                                    style={{
                                                                                        fontSize:
                                                                                            '12px',
                                                                                        marginRight:
                                                                                            '12px',
                                                                                        fill:
                                                                                            '#d8dde7',
                                                                                    }}
                                                                                />
                                                                            )}
                                                                            <strong className="legend-class">
                                                                                {
                                                                                    item.title
                                                                                }
                                                                            </strong>
                                                                        </li>
                                                                    </ul>
                                                                </a>
                                                            )
                                                        )}
                                                </ul>
                                            </Typography>
                                        </AccordionDetails>
                                    </Accordion>
                                </div>
                            </ModuleListBody>
                        </ModuleListContent>
                    )}
                    {!loadingClass && (
                        <div
                            style={{
                                width: '100%',
                                height: '100%',
                                display: 'flex',
                                flexDirection: 'column',
                                padding: '20px',
                                boxShadow: 'rgb(0 0 0 / 10%) 0px 1px 2px',
                                background: '#fff',
                            }}
                            className="skeletonMaterials"
                        >
                            <Skeleton
                                variant="rect"
                                style={{ width: '60%', marginBottom: '15px' }}
                                height={30}
                            />
                            <Skeleton
                                variant="rect"
                                style={{ width: '78%' }}
                                height={30}
                            />

                            <Skeleton
                                variant="rect"
                                style={{ width: '90%', marginTop: '20px' }}
                                height={50}
                            />
                            <Skeleton
                                variant="rect"
                                style={{ width: '90%', marginTop: '20px' }}
                                height={50}
                            />
                        </div>
                    )}
                </ModuleListDetails>
            </ModuleContent>
        </ModuleContainer>
    );
};
