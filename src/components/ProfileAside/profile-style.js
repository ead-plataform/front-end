import styled from 'styled-components';

export const ProfileContainer = styled.div`
    width: 100%;
    max-width: 285px;
    height: 445px;
    box-shadow: rgb(0 0 0 / 10%) 0px 1px 2px;
    display: flex;
    flex-direction: column;

    @media only screen and (max-width: 930px) {
        & {
            max-width: none;
        }
    }
`;

export const ProfileHeader = styled.header`
    width: 100%;
    height: 200px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    justify-content: space-around;
`;

export const ProfileContent = styled.div`
    width: 100%;
    max-width: 128px;
    height: 128px;
    position: relative;
`;

export const Profile = styled.figure`
    width: 100%;
    height: inherit;
    border-radius: 50%;
    border: 11px solid rgb(240, 241, 247);
    background-size: cover;
    background-position: center;
    margin: 0px;
`;

export const ProfileButtonEdit = styled.label`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 0px;
    right: -5px;
    cursor: pointer;
    background-color: rgb(250, 250, 252);
    box-shadow: rgb(0 0 0 / 10%) 0px 4px 8px;
`;

export const ProfileName = styled.strong`
    font-size: 16px;
    color: #3a3e47;
    font-family: 'Nunito', sans-serif;
`;

export const ProfileNav = styled.nav`
    flex: 1;
    display: flex;
    flex-direction: column;

    li {
        display: flex;
        justify-content: center;
    }

    a {
        width: 90%;
        display: flex;
        align-items: center;
        position: relative;
        padding: 10px 0px;
        color: #3a3e47;
        font-size: 16px;
        font-family: 'Nunito', sans-serif;
        border-bottom: 1px solid #fafbfc;

        strong {
            font-size: smaller;
        }
    }
    .arrow-right {
        font-size: 15px;
    }
`;

export const ProfileNavTitle = styled.strong`
    width: 100%;
    padding: 10px;
    font-family: 'Nunito', sans-serif;
    color: #3a3e47;
    margin-bottom: 10px;
`;
