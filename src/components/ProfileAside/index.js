import { useState, useEffect } from 'react';
import CreateOutlinedIcon from '@material-ui/icons/CreateOutlined';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import ArrowForwardIosOutlinedIcon from '@material-ui/icons/ArrowForwardIosOutlined';
import HttpsOutlinedIcon from '@material-ui/icons/HttpsOutlined';
import { Link } from 'react-router-dom';
import {
    ProfileContainer,
    ProfileHeader,
    ProfileNav,
    ProfileContent,
    Profile,
    ProfileButtonEdit,
    ProfileName,
    ProfileNavTitle,
} from './profile-style';
import { AppUserValue } from '../../contexts/UserContext';
import api from '../../services/api';

export default () => {
    const [state, dispatch] = AppUserValue();
    const [name] = useState(state.user.name);
    const url = window.location.pathname;

    useEffect(() => {}, [state.user]);

    const defineProfile = async (prof) => {
        const formData = new FormData();
        formData.append('figure', prof.target.files[0]);
        const setProfile = await api.setProfile('profile', formData);

        dispatch({
            type: 'addprofile',
            profile: `${setProfile}`,
        });
    };

    return (
        <ProfileContainer>
            <ProfileHeader>
                <ProfileContent>
                    <Profile
                        style={{
                            backgroundImage: `url(${state.user.profile})`,
                        }}
                    />
                    <ProfileButtonEdit htmlFor="editProfile">
                        <CreateOutlinedIcon />
                        <input
                            type="file"
                            id="editProfile"
                            style={{ display: 'none' }}
                            onChange={defineProfile}
                        />
                    </ProfileButtonEdit>
                </ProfileContent>
                <ProfileName>{name}</ProfileName>
            </ProfileHeader>
            <ProfileNav>
                <ProfileNavTitle>Minha Conta</ProfileNavTitle>
                <ul>
                    <li
                        style={{
                            backgroundColor:
                                url === '/app/profile/me' ? '#F0F1F7' : '#fff',
                        }}
                    >
                        <Link to="/app/profile/me">
                            <AccountCircleOutlinedIcon
                                style={{ marginRight: '15px' }}
                            />
                            <strong>Meus Dados</strong>
                            <ArrowForwardIosOutlinedIcon
                                style={{ position: 'absolute', right: '0px' }}
                                className="arrow-right"
                            />
                        </Link>
                    </li>
                    <li
                        style={{
                            backgroundColor:
                                url === '/app/profile/me/password'
                                    ? '#F0F1F7'
                                    : '#fff',
                        }}
                    >
                        <Link to="/app/profile/me/password">
                            <HttpsOutlinedIcon
                                style={{ marginRight: '15px' }}
                            />
                            <strong>Senha</strong>
                            <ArrowForwardIosOutlinedIcon
                                style={{ position: 'absolute', right: '0px' }}
                                className="arrow-right"
                            />
                        </Link>
                    </li>
                </ul>
            </ProfileNav>
        </ProfileContainer>
    );
};
