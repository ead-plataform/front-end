import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import { Link } from 'react-router-dom';
import { AppUseValue } from '../../contexts/AppContext';

const useStyles = makeStyles({
    root: {
        width: '100%',
        height: 'max-content',
        maxWidth: 350,
        marginBottom: '20px',
        marginRight: '20px',
    },
    media: {
        height: 100,
    },
    progress: {
        width: '80%',
        height: '5px',
        borderRadius: '2px',
        background: '#d8dde7',
    },
    progressPercentu: {
        height: 'inherit',
        borderRadius: '2px',
        background: '#34325e',
    },
    modify: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
});

const CardCourse = ({ image, id, slug, title, modules, concluded }) => {
    const classes = useStyles();
    const [state, dispatch] = AppUseValue(); // eslint-disable-line
    const progress = (modules / 100) * concluded;
    const url = window.location.pathname;
    const setCourse = () => {
        dispatch({ type: 'idcourse', id });
        dispatch({ type: 'titlecourse', title });
        dispatch({ type: 'slugcourse', slug });
    };

    return (
        <Link
            to={`${url}/${slug}`}
            className={classes.root}
            onClick={setCourse}
        >
            <Card>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={image}
                        title={title}
                        style={{ backgroundSize: '40%' }}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h4">
                            {title}
                        </Typography>
                        <div
                            className={classes.modify}
                            style={{ marginTop: '10px' }}
                        >
                            <div
                                style={{
                                    width: 'max-content',
                                    padding: '10px 20px',
                                    fontSize: '12px',
                                    background: '#34325e',
                                    color: '#fff',
                                    borderRadius: '3px',
                                }}
                            >
                                {progress !== 0 ? 'Continuar' : 'Iniciar'}
                            </div>
                        </div>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Link>
    );
};

export default CardCourse;
