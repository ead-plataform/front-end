import styled from 'styled-components';

export const NavigationContainer = styled.div`
    width: 100%;
    max-width: 1120px;
    height: 90px;
    display: flex;
    align-items: center;
    margin-top: 20px;

    @media only screen and (max-width: 675px) {
        & {
            margin-bottom: 20px;
        }
    }
`;

export const NavigationContent = styled.div`
    width: 100%;
    height: 70px;
    display: flex;
    align-items: center;
    transition: color 0.3s linear;
    border-radius: 5px;
    position: relative;
    justify-content: space-between;

    .header--nav-link {
        font-family: 'Nunito', sans-serif;
        font-size: smaller;
        color: rgb(124, 129, 149);
    }
    .header--nav-link:hover {
        color: var(--color-secundary);
    }
    .header--nav-link::first-letter {
        text-transform: uppercase;
    }
    .active {
        color: var(--color-link);
    }

    .abreviation {
        width: 200px;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
    }

    @media only screen and (max-width: 600px) {
        & {
            flex-wrap: wrap;
        }
    }
`;
