import { NavigationContainer, NavigationContent } from './navigation-style';

export default ({ children, style, child }) => (
    <NavigationContainer style={style}>
        <NavigationContent style={child}>{children}</NavigationContent>
    </NavigationContainer>
);
