import styled from 'styled-components';

export const SkeletonContainer = styled.div`
    width: 100%;
    max-width: ${(props) => props.width}px;
    height: ${(props) => props.height}px;
    margin-bottom: ${(props) => props.margin}px;
    margin-right: 30px;
`;
