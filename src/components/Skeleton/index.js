import Skeleton from '@material-ui/lab/Skeleton';
import { SkeletonContainer } from './skeleton-style';

export default ({ width, height, margin }) => (
    <SkeletonContainer width={width} height={height} margin={margin}>
        <Skeleton variant="rect" height={90} />
        <Skeleton
            variant="text"
            style={{ maxWidth: '190px', marginTop: '10px' }}
        />
        <div style={{ display: 'flex', width: '100%', marginTop: '20px' }}>
            <Skeleton
                variant="text"
                style={{ width: '80%', marginRight: '10px' }}
            />
            <Skeleton variant="text" style={{ flex: '1' }} />
        </div>
        <div
            style={{
                display: 'flex',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: '20px',
            }}
        >
            <Skeleton variant="text" style={{ width: '30%' }} />
            <Skeleton variant="text" style={{ width: '30%' }} />
        </div>
    </SkeletonContainer>
);
