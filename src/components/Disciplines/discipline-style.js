import styled from 'styled-components';

export const DisciplinesContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 0px 10px;
`;

export const DisciplinesContent = styled.div`
    width: 100%;
    max-width: 1120px;
    flex: 1;
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    flex-wrap: wrap;
    align-content: flex-start;
    padding-top: 20px;

    @media only screen and (max-width: 1050px) {
        justify-content: space-between;
    }
`;
