import { useState, useEffect } from 'react';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import { Link } from 'react-router-dom';
import { DisciplinesContainer, DisciplinesContent } from './discipline-style';

import NavigationMenu from '../NavigationMenu';
import CardDiscipline from '../CardDiscipline';

import api from '../../services/api';

import { AppUseValue } from '../../contexts/AppContext';

import { ButtonAddDiscipline } from '../Admin/ButtonSeetingCourse';
import SkeletonDiscipline from '../Skeleton';
import { unLockAdmin } from '../../helpers/auth-unlock-admin';

const Disciplines = () => {
    const [loading, setLoading] = useState(false);
    const [disciplines, setDiscipline] = useState([]);
    const [state] = AppUseValue();
    const [titleCourse, setTitleCourse] = useState('');
    const isAdmin = unLockAdmin();

    const catchDiscipline = async () => {
        const discipline = await api.catchDiscipline(
            'discipline',
            state.course.id
        );
        setTitleCourse(state.course.title);
        setDiscipline(discipline);
        setLoading(true);
    };

    document.title = 'Modulos';

    useEffect(() => {
        catchDiscipline();
    }, []);

    return (
        <>
            <DisciplinesContainer>
                <NavigationMenu child={{ justifyContent: 'flex-start' }}>
                    <Link
                        className="header--nav-link"
                        to="/app/cursos?page=1&search="
                    >
                        Cursos:
                    </Link>
                    <KeyboardArrowRightIcon fontSize="small" />
                    <Link
                        className="header--nav-link active"
                        to={`/app/cursos/${state.course.slug}`}
                    >
                        {titleCourse}
                    </Link>
                </NavigationMenu>
                <DisciplinesContent>
                    {isAdmin && <ButtonAddDiscipline />}
                    {loading &&
                        disciplines.map((item, key) => {
                            if (
                                item.slug === 'biologia' ||
                                item.slug === 'quimica' ||
                                item.slug === 'fisica'
                            ) {
                                return (
                                    <CardDiscipline
                                        name={item.title}
                                        modules={item.qt_modules}
                                        top="#7dc7ba"
                                        bottom="#41766a"
                                        slug={item.slug}
                                        id={item.id}
                                        watching={item.qt_concluded}
                                key={key} // eslint-disable-line
                                    />
                                );
                            }
                            if (
                                item.slug === 'geografia' ||
                                item.slug === 'filosofia' ||
                                item.slug === 'sociologia' ||
                                item.slug === 'historia'
                            ) {
                                return (
                                    <CardDiscipline
                                        name={item.title}
                                        modules={item.qt_modules}
                                        top="#FAE278"
                                        bottom="#998545"
                                        slug={item.slug}
                                        id={item.id}
                                        watching={item.qt_concluded}
                                key={key} // eslint-disable-line
                                    />
                                );
                            }
                            if (
                                item.slug === 'portugues' ||
                                item.slug === 'literatura' ||
                                item.slug === 'interpretacao' ||
                                item.slug === 'redacao'
                            ) {
                                return (
                                    <CardDiscipline
                                        name={item.title}
                                        modules={item.qt_modules}
                                        top="#FF7272"
                                        bottom="#994146"
                                        slug={item.slug}
                                        id={item.id}
                                        watching={item.qt_concluded}
                                        key={key} // eslint-disable-line
                                    />
                                );
                            }
                            if (item.slug === 'matematica') {
                                return (
                                    <CardDiscipline
                                        name={item.title}
                                        modules={item.qt_modules}
                                        top="#FC9F5B"
                                        bottom="#995A33"
                                        slug={item.slug}
                                        id={item.id}
                                        watching={item.qt_concluded}
                                        key={key} // eslint-disable-line
                                    />
                                );
                            }
                            return (
                                <CardDiscipline
                                    name={item.title}
                                    modules={item.qt_modules}
                                    top="#FC9F5B"
                                    bottom="#995A33"
                                    slug={item.slug}
                                    id={item.id}
                                    watching={item.qt_concluded}
                                        key={key} // eslint-disable-line
                                />
                            );

                            // return null;
                        })}
                    {!loading && (
                        <>
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                            <SkeletonDiscipline
                                width="200"
                                height="150"
                                margin="80"
                            />
                        </>
                    )}
                </DisciplinesContent>
            </DisciplinesContainer>
        </>
    );
};

export default Disciplines;
