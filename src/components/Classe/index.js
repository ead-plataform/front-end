import { useState, useEffect, useRef } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import Player from '@vimeo/player';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import CloseIcon from '@material-ui/icons/Close';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import {
    ClassesContainer,
    ClassesContent,
    ClassesContentHeader,
    ClassesContentBody,
    ClassesArea,
    ClassesVideoPlayer,
    ClassesVideoList,
    ClassesVideoListHeader,
    ClassesVideoListTitle,
    ClassesVideoListBody,
    ClassesModalContainer,
    ClassesModalHeader,
    ClassesModalBody,
} from './classe-style';
import { Button } from '../Button';
import NavigationMenu from '../NavigationMenu';
import Modal from '../Modal';
import api from '../../services/api';
import { AppUseValue } from '../../contexts/AppContext';
import { LoginMultipleBlocked } from '../../helpers/auth-locked-multiple-access';

export default () => {
    const openModal = localStorage.getItem('modal');
    const [state, dispatch] = AppUseValue();
    const [classe, setClasse] = useState([]);
    const [loading, setloadingClass] = useState(false);
    const [titleClass, setTitleClass] = useState('');
    const [slugClass, setSlug] = useState('');
    const [urlClass, setUrlClass] = useState('');
    const [nextClasse, setNextClasse] = useState(false);
    const [render, setRender] = useState('');
    const [open, setOpen] = useState(!openModal);
    const history = useHistory();
    const iframe = useRef();
    const { aula } = useParams();

    document.title = titleClass;

    const catchPlayer = (aulaId, idClass, viewed) => {
        const player = new Player(iframe.current, { id: aulaId });

        player.on('ended', async () => {
            if (!viewed) {
                await api.addHistoric('/historic', {
                    id_class: idClass,
                    moduleId: state.course.moduleId.id,
                });
                await api.setProgressModule('/progress', {
                    totalItems: state.course.moduleId.qtMaterials,
                    completedItems: '1',
                    disciplineId: state.course.discipline.id,
                    moduleId: state.course.moduleId.id,
                });
            }
        });
    };

    const catchClass = async () => {
        const classes = await api.catchClass(
            'classe',
            state.course.moduleId.id
        );
        dispatch({ type: 'setClasses', classes });
        setClasse(classes);
        const title = classes.find((element) => element.slug === aula);
        if (title === undefined) window.location.href = '/app';
        setUrlClass(title.url);
        setloadingClass(true);
        setTitleClass(title.title);
        setSlug(title.slug);
        catchPlayer(title.url, title.id, title.viewed);
    };

    useEffect(() => {
        LoginMultipleBlocked();
        catchClass();
    }, [render]);

    const exitedModal = () => {
        localStorage.setItem('modal', true);
        setOpen(false);
    };

    return (
        <ClassesContainer>
            <Modal open={open} handleClose={exitedModal}>
                <ClassesModalContainer>
                    <ClassesModalHeader>
                        <SentimentVerySatisfiedIcon fontSize="large" />
                        <p>Olá! Tudo Bom? Espero que sim.</p>
                        <CloseIcon
                            style={{
                                position: 'absolute',
                                right: '10px',
                                top: '8px',
                                cursor: 'pointer',
                            }}
                            onClick={exitedModal}
                            color="primary"
                        />
                    </ClassesModalHeader>
                    <ClassesModalBody>
                        <p>
                            Então vou ser o mais breve possível! Se você
                            percebeu os cursos que estão na plataforma não estão
                            completo! Mais nos estamos inserindo as video aula
                            semanalmente até os cursos ficarem completos
                        </p>
                        <p>
                            Só vai continuar acessando a plataforma quem estiver
                            no nosso grupo no telegram! Clicando no Botão abaixo
                            você será redirecionado para o nosso grupo!
                        </p>
                        <a
                            href="https://t.me/joinchat/nZFjRjEpdq9iMWVh"
                            target="_blank"
                            rel="noreferrer"
                        >
                            Entrar no Grupo
                        </a>
                    </ClassesModalBody>
                </ClassesModalContainer>
            </Modal>
            <ClassesContent>
                <ClassesContentHeader style={{ padding: '0 0.7rem' }}>
                    <NavigationMenu
                        child={{ width: '100%', justifyContent: 'flex-start' }}
                        style={{ height: '60px', marginTop: '0px' }}
                    >
                        <Link
                            to="/app/cursos?page=1&search="
                            className="header--nav-link"
                        >
                            Cursos:
                        </Link>
                        <KeyboardArrowRightIcon
                            style={{
                                width: '15px',
                                height: '15px',
                                marginRight: '5px',
                            }}
                        />
                        <Link
                            to={`/app/cursos/${state.course.slug}`}
                            className="header--nav-link"
                        >
                            {state.course.title}
                        </Link>
                        <KeyboardArrowRightIcon
                            style={{
                                width: '15px',
                                height: '15px',
                                marginRight: '5px',
                            }}
                        />
                        <Link
                            to={`/app/cursos/${state.course.slug}/${state.course.discipline.slug}`}
                            className="header--nav-link"
                        >
                            {state.course.discipline.title}
                        </Link>
                        <KeyboardArrowRightIcon
                            style={{
                                width: '15px',
                                height: '15px',
                                marginRight: '5px',
                            }}
                        />
                        <Link
                            to={`/app/cursos/${state.course.slug}/${state.course.discipline.slug}`}
                            className="header--nav-link"
                        >
                            {state.course.moduleId.title}
                        </Link>
                        <KeyboardArrowRightIcon
                            style={{
                                width: '15px',
                                height: '15px',
                                marginRight: '5px',
                            }}
                        />
                        <Link
                            to={`${slugClass}`}
                            className="header--nav-link abreviation active"
                        >
                            {titleClass}
                        </Link>
                    </NavigationMenu>
                </ClassesContentHeader>
                <ClassesContentBody>
                    <ClassesArea>
                        <ClassesVideoPlayer>
                            {loading && urlClass && (
                                <iframe
                                    ref={iframe}
                                    src={`https://player.vimeo.com/video/${urlClass}`}
                                    frameBorder="0"
                                    allow="autoplay; fullscreen; picture-in-picture"
                                    allowFullScreen
                                    title="title"
                                />
                            )}
                        </ClassesVideoPlayer>
                        <ClassesVideoList>
                            <ClassesVideoListHeader>
                                <ClassesVideoListTitle>
                                    Vídeos Relacionados
                                </ClassesVideoListTitle>
                            </ClassesVideoListHeader>
                            <ClassesVideoListBody>
                                {loading &&
                                    classe.map((item, index) => (
                                        <Timeline
                                            style={{ padding: '0px' }}
                                            key={item.id}
                                        >
                                            <TimelineItem>
                                                <TimelineSeparator>
                                                    <TimelineDot
                                                        style={{
                                                            background:
                                                                (item.slug ===
                                                                    slugClass &&
                                                                    '#595bf9') ||
                                                                (item.viewed
                                                                    ? '#50C79D'
                                                                    : '#d8dde7'),
                                                        }}
                                                    />
                                                    {classe.length - 1 >
                                                        index && (
                                                        <TimelineConnector
                                                            style={{
                                                                background: item.viewed
                                                                    ? '#50C79D'
                                                                    : '#d8dde7',
                                                            }}
                                                        />
                                                    )}
                                                </TimelineSeparator>
                                                <a
                                                    className="class-title"
                                                    href={item.slug}
                                                    style={{
                                                        color:
                                                            (item.slug ===
                                                                slugClass &&
                                                                '#595bf9') ||
                                                            (item.viewed &&
                                                                '#50c79d'),

                                                        fontSize: '15px',
                                                    }}
                                                >
                                                    <TimelineContent>
                                                        {item.title}
                                                    </TimelineContent>
                                                </a>
                                            </TimelineItem>
                                        </Timeline>
                                    ))}
                            </ClassesVideoListBody>
                        </ClassesVideoList>
                    </ClassesArea>
                </ClassesContentBody>
            </ClassesContent>
        </ClassesContainer>
    );
};
