import styled from 'styled-components';

export const ClassesContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
`;

export const ClassesContent = styled.div`
    width: 85%;
    display: flex;
    flex-direction: column;

    ul > li::before {
        content: none;
        padding: 0px;
    }
    ul > li {
        min-height: 55px;
    }
    a {
        color: #000;
    }
    a:hover {
        transition: color 0.3s ease-in-out;
        color: #595bf9 !important;
    }
    a::active {
        color: red !important;
    }
`;

export const ClassesContentHeader = styled.div`
    width: 100%;
    height: max-content;
    margin-top: 35px;
`;

export const ClassesContentBody = styled.div`
    flex: 1;
`;

export const ClassesArea = styled.div`
    width: 100%;
    height: 400px;
    display: flex;
    justify-content: space-between;

    @media only screen and (max-width: 1000px) {
        & {
            flex-direction: column;
        }
    }
`;

export const ClassesVideoPlayer = styled.div`
    width: 100%;
    max-width: 740px;
    position: relative;

    & > iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    @media only screen and (max-width: 1190px) {
        & {
            width: 70%;
        }
    }
    @media only screen and (max-width: 1000px) {
        & {
            width: 100%;
            height: inherit;
            margin-bottom: 30px;
        }
    }
`;
export const ClassesVideoList = styled.div`
    width: 35%;
    padding: 0px 20px;
    display: flex;
    flex-direction: column;

    overflow-y: auto;

    &::-webkit-scrollbar {
        background-color: transparent;
        width: 6px;
        height: 7px;
    }
    &::-webkit-scrollbar-button {
        display: none;
    }
    &::-webkit-scrollbar-thumb {
        background-color: rgb(186, 186, 192);
        border-radius: 6px;
        border: 5px solid transparent;
    }
    &::-webkit-scrollbar-track {
        border-radius: 6px;
        background-color: rgba(0, 0, 0, 0.03);
    }

    @media only screen and (max-width: 1190px) {
        & {
            width: 30%;
        }
    }
    @media only screen and (max-width: 1000px) {
        & {
            width: 100%;
            padding: 0px;
        }
    }
`;
export const ClassesVideoListHeader = styled.header`
    width: 100%;
    margin-bottom: 30px;
`;
export const ClassesVideoListTitle = styled.strong`
    font-family: 'Nunito', sans-serif;
    font-size: 18px;
    font-weight: 600;
`;
export const ClassesVideoListBody = styled.div`
    width: 100%;
    max-width: 320px;
    height: 100%;
`;
export const ClassesVideo = styled.a`
    display: block;
    width: 100%;
    display: flex;
    align-items: center;
    cursor: pointer;
    padding: 15px;
    position: relative;
    border-bottom: 2px solid #fafbfc;
    transition: all 0.3s linear;

    &:hover {
        background: var(--color-primary);
    }

    &.focused {
        background: var(--color-primary);
    }
`;

export const ClassesVideoTitle = styled.strong`
    font-family: 'Nunito', sans-serif;
    font-size: small;
    color: var(--color-secundary);
    margin-left: 15px;

    ${ClassesVideo}.focused & {
        color: #fff;
    }
    ${ClassesVideo}:hover & {
        color: #fff;
    }
`;

export const ClassesModalContainer = styled.div`
    width: 100%;
    max-width: 400px;
    height: max-content;
`;

export const ClassesModalHeader = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    padding-top: 20px;

    p {
        margin: 0px;
        font-family: 'Nunito Sans', sans-serif;
        font-size: 14px;
        color: rgb(80, 84, 98);
        margin-left: 20px;
    }
`;

export const ClassesModalBody = styled.div`
    width: 100%;
    padding: 10px;
    padding-bottom: 10px;
    display: flex;
    flex-direction: column;

    p {
        margin-bottom: 15px;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        color: rgb(80, 84, 98);
        text-align: justify;
        line-height: 25px;
    }
    a {
        background-color: var(--color-link);
        padding: 10px;
        margin-top: 20px;
        text-align: center;
        border-radius: 4px;
    }
`;
