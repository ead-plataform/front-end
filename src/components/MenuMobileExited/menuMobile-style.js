import styled from 'styled-components';

export const AppContainerVisible = styled.div`
    display:none;
    width: 100%;
    height  100vh;
    position: fixed;
    background: rgba(0, 0, 0, 0.2);
    z-index:2;

    @media only screen and (max-width:1030px) {
        display: ${(props) => (props.menuWidth ? 'none' : 'block')};
    }
}
`;
