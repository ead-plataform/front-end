import { useEffect } from 'react';
import { AppUseValue } from '../../contexts/AppContext';
import { AppContainerVisible } from './menuMobile-style';

const MenuMobileExited = () => {
    const [state, dispatch] = AppUseValue();

    const handleClose = () => {
        const menu = state.menu.menuOpened;
        dispatch({ type: 'setMenu', menuOpened: !menu });
    };

    useEffect(() => {}, [state.menu.menuOpened]);

    return (
        <>
            <AppContainerVisible
                onClick={handleClose}
                menuWidth={state.menu.menuOpened}
            />
        </>
    );
};

export default MenuMobileExited;
