import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    ResponsiveContainer,
} from 'recharts';
import { Link } from 'react-router-dom';
import {
    DashBoardMain,
    DashBoardContent,
    DashGraphics,
    DashPhotoStudents,
    DashBoardContentLeft,
    DashGraphicsTitle,
    DashPhotoStudentsHeader,
    DashAreaCourseBody,
    DashRadiusCardCourse,
} from './dashboard-style';

const data = [
    {
        name: '22/04/2021',
        uv: 1,
    },
    {
        name: '23/04/2021',
        uv: 1,
    },
    {
        name: '24/04/2021',
        uv: 2,
    },
    {
        name: '25/04/2021',
        uv: 3,
    },
    {
        name: '26/04/2021',
        uv: 4,
    },
];

document.title = 'Home';

export default () => (
    <DashBoardMain>
        <DashBoardContent>
            <DashBoardContentLeft>
                <DashGraphics>
                    <DashGraphicsTitle>
                        Gráfico de Progresso (Aulas assistidas)
                    </DashGraphicsTitle>
                    <br />
                    <ResponsiveContainer width="100%" height={200}>
                        <LineChart
                            width={500}
                            height={200}
                            data={data}
                            syncId="anyId"
                            margin={{
                                top: 10,
                                right: 30,
                                left: 0,
                                bottom: 0,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis
                                dataKey="name"
                                style={{
                                    fontFamily: 'Roboto',
                                    fontSize: 'smaller',
                                }}
                            />
                            <YAxis
                                style={{
                                    fontFamily: 'Roboto',
                                    fontSize: 'smaller',
                                }}
                            />
                            <Tooltip />
                            <Line
                                type="monotone"
                                dataKey="uv"
                                stroke="#595bf9"
                                fill="#595bf9 "
                                name="Aulas Assistidas"
                            />
                        </LineChart>
                    </ResponsiveContainer>
                </DashGraphics>
            </DashBoardContentLeft>
        </DashBoardContent>
    </DashBoardMain>
);
