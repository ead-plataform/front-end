import styled from 'styled-components';

export const DashBoardMain = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
`;

export const DashBoardContent = styled.div`
    width: 85%;
    height: 100%;
    padding: 50px 0 20px 0;
    display: flex;
`;

export const DashBoardContentLeft = styled.div`
    width: 100%;
    height: inherit;
    display: flex;
    flex-direction: column;
`;

export const DashGraphics = styled.section`
    width: 100%;
    flex: 1;
    & > svg {
        position: absolute;
        left: -25px;
    }
`;

export const DashGraphicsTitle = styled.h2`
    font-family: 'Roboto', sans-serif;
    font-size: 15px;
`;

export const DashPhotoStudents = styled.section`
    height: 250px;
`;

export const DashPhotoStudentsHeader = styled.div`
    width: 100%;
    height: 40px;
    display: flex;
    justify-content: space-between;
    .view-all--courses {
        color: var(--color-primary);
        font-size: smaller;
    }
`;

export const DashAreaCourseBody = styled.div`
    width: 100%;
    padding: 10px;
    display: flex;
    flex-wrap: wrap;
`;

export const DashRadiusCardCourse = styled.a`
    width: 100%;
    display: block;
    max-width: 70px;
    height: 70px;
    padding: 5px;
    border-radius: 50%;
    background-image: url(${(props) => props.image});
    background-position: center;
    background-size: 75%;
    background-repeat: no-repeat;
    box-shadow: rgb(0 0 0 / 10%) 0px 1px 2px;
    margin: 0 15px 15px 0;
    cursor: pointer;
`;
