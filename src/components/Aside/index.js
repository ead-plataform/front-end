import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { AppUseValue } from '../../contexts/AppContext';
import {
    AsideContainer,
    AsideBanner,
    AsideBannerFigure,
    AsideNav,
} from './aside-style';

const Aside = () => {
    const url = window.location.pathname;

    const [state] = AppUseValue();
    const { menuOpened } = state.menu;

    useEffect(() => {}, [menuOpened]);

    return (
        <>
            <AsideContainer menuWidth={menuOpened}>
                <AsideBanner>
                    <AsideBannerFigure src="/assets/image/brain.svg" />
                </AsideBanner>
                <AsideNav menuWidth={menuOpened}>
                    <ul className="app--menu">
                        <li
                            className={`${
                                url === '/app' && 'app--menu-borderactive'
                            }`}
                        >
                            <Link
                                to="/app"
                                className={`app--menu-action
                                ${url === '/app' && 'app--menu-active'}`}
                            >
                                <svg
                                    version="1.1"
                                    id="Capa_1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="20px"
                                    height="20px"
                                    className={`app--menu-icon ${
                                        url === '/app' && 'app--menu-spanactive'
                                    }`}
                                    x="0px"
                                    y="0px"
                                    viewBox="0 0 306.773 306.773"
                                    xmlSpace="preserve"
                                >
                                    <g>
                                        <path
                                            d="M302.93,149.794c5.561-6.116,5.024-15.49-1.199-20.932L164.63,8.898
		c-6.223-5.442-16.2-5.328-22.292,0.257L4.771,135.258c-6.092,5.585-6.391,14.947-0.662,20.902l3.449,3.592
		c5.722,5.955,14.971,6.665,20.645,1.581l10.281-9.207v134.792c0,8.27,6.701,14.965,14.965,14.965h53.624
		c8.264,0,14.965-6.695,14.965-14.965v-94.3h68.398v94.3c-0.119,8.264,5.794,14.959,14.058,14.959h56.828
		c8.264,0,14.965-6.695,14.965-14.965V154.024c0,0,2.84,2.488,6.343,5.567c3.497,3.073,10.842,0.609,16.403-5.513L302.93,149.794z"
                                        />
                                    </g>
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                </svg>

                                <span
                                    className={`app--menu-span ${
                                        url === '/app' && 'app--menu-spanactive'
                                    }`}
                                >
                                    Home
                                </span>
                            </Link>
                        </li>
                        <li
                            className={`${
                                /app\/cursos\/?(\W+)?/.test(url) &&
                                'app--menu-borderactive'
                            }`}
                        >
                            <Link
                                to="/app/cursos?page=1&search="
                                className={`app--menu-action
                                ${
                                    /app\/cursos\/?(\W+)?/.test(url) &&
                                    'app--menu-active'
                                }`}
                            >
                                <svg
                                    fill="#fff"
                                    version="1.1"
                                    id="Capa_1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    x="0px"
                                    y="0px"
                                    width="22px"
                                    height="22px"
                                    viewBox="0 0 288.147 288.147"
                                    xmlSpace="preserve"
                                    className={`app--menu-icon ${
                                        /app\/cursos\/?(\W+)?/.test(url) &&
                                        'app--menu-spanactive'
                                    }`}
                                >
                                    <g>
                                        <g>
                                            <path
                                                d="M142.45,174.613c-4.645,0-11.495-0.514-17.779-2.926l-50.271-19.366H49.774v30.162c0,9.274,6.9,19.802,15.405,23.499
			l60.872,26.496c8.505,3.691,22.312,3.707,30.826,0.036l61.536-26.574c8.508-3.671,15.41-14.183,15.41-23.457v-30.162h-27.175
			l-44.547,18.78C156.742,173.365,149.756,174.613,142.45,174.613z"
                                            />
                                            <path
                                                d="M6.475,112.944l121.222,46.709c8.661,3.329,22.603,3.112,31.152-0.492l115.768-48.801v71.999l-7.151,23.866h20.682
			l-7.399-24.114V107.45h-0.208c4.997-3.449,3.832-7.747-3.567-10.393L159.196,55.146c-8.74-3.117-22.859-2.985-31.545,0.277
			L6.529,100.99C-2.157,104.258-2.178,109.612,6.475,112.944z"
                                            />
                                        </g>
                                    </g>
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                    <g />
                                </svg>

                                <span
                                    className={`app--menu-span ${
                                        /app\/cursos\/?(\W+)?/.test(url) &&
                                        'app--menu-spanactive'
                                    }`}
                                >
                                    Meus cursos
                                </span>
                            </Link>
                        </li>
                        <li
                            className={`${
                                url === '/app/materiais' &&
                                'app--menu-borderactive'
                            }`}
                        >
                            <Link
                                to="/app/materiais"
                                className={`app--menu-action
                                ${
                                    url === '/app/materiais' &&
                                    'app--menu-active'
                                }`}
                            >
                                <svg
                                    height="22px"
                                    viewBox="0 0 480.00958 480"
                                    width="22px"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className={`app--menu-icon ${
                                        url === '/app/materiais' &&
                                        'app--menu-spanactive'
                                    }`}
                                >
                                    <path d="m128 104.003906v-32c0-4.417968-3.582031-8-8-8h-112c-4.417969 0-8 3.582032-8 8v32zm0 0" />
                                    <path d="m0 120.003906v264h128v-264zm96 104h-64c-4.417969 0-8-3.582031-8-8v-64c0-4.417968 3.582031-8 8-8h64c4.417969 0 8 3.582032 8 8v64c0 4.417969-3.582031 8-8 8zm0 0" />
                                    <path d="m0 432.003906v40c0 4.417969 3.582031 8 8 8h112c4.417969 0 8-3.582031 8-8v-40zm0 0" />
                                    <path d="m0 400.003906h128v16h-128zm0 0" />
                                    <path d="m144 400.003906h104v16h-104zm0 0" />
                                    <path d="m248 32.003906v-24c0-4.417968-3.582031-7.99999975-8-7.99999975h-88c-4.417969 0-8 3.58203175-8 7.99999975v24zm0 0" />
                                    <path d="m144 88.003906h104v296h-104zm0 0" />
                                    <path d="m144 432.003906v40c0 4.417969 3.582031 8 8 8h88c4.417969 0 8-3.582031 8-8v-40zm0 0" />
                                    <path d="m144 48.003906h104v24h-104zm0 0" />
                                    <path d="m263.808594 165.667969 4.28125 16.433593 135.484375-36.128906-4.285157-16.433594zm0 0" />
                                    <path d="m330.664062 421.949219 135.480469-36.128907-8.578125-32.886718-135.488281 36.128906zm0 0" />
                                    <path d="m453.527344 337.453125-45.910156-176-135.488282 36.128906 45.910156 176zm0 0" />
                                    <path d="m259.769531 150.1875 135.476563-36.125-11.484375-44.058594c-1.183594-4.265625-5.542969-6.816406-9.839844-5.757812l-120 32c-4.238281 1.160156-6.765625 5.5-5.683594 9.757812zm0 0" />
                                    <path d="m470.183594 401.300781-135.488282 36.128907 9.542969 36.574218c.554688 2.0625 1.90625 3.820313 3.761719 4.882813 1.207031.730469 2.589844 1.117187 4 1.117187.699219 0 1.398438-.082031 2.078125-.238281l120-32c4.238281-1.160156 6.765625-5.503906 5.683594-9.761719zm0 0" />
                                </svg>
                                <span
                                    className={`app--menu-span ${
                                        url === '/app/materiais' &&
                                        'app--menu-spanactive'
                                    }`}
                                >
                                    Material
                                </span>
                            </Link>
                        </li>
                    </ul>
                </AsideNav>
            </AsideContainer>
        </>
    );
};

export default Aside;
