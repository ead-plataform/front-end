import styled from 'styled-components';

export const AsideContainer = styled.aside`
    width: ${(props) => (props.menuWidth ? '75px' : '220px')};
    height: 100vh;
    border-right: 2px solid #fafbfc;

    @media only screen and (max-width: 1030px) {
        & {
            width: 250px;
            position: absolute;
            z-index: 10;
            background: #fff;
            display: ${(props) => (props.menuWidth ? 'none' : 'block')};
        }
    }
`;

export const AsideBanner = styled.div`
    width: 100%;
    height: 80px;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const AsideBannerFigure = styled.img`
    width: 100%;
    max-width: 40px;
    height: 40px;
`;

export const AsideNav = styled.nav`
    width: 100%;
    height: auto;
    margin-top: 40px;

    .app--menu li {
        width: 100%;
        height: 50px;
        display: flex;
        align-items: center;
        position: relative;
    }
    .app--menu li .app--menu-action {
        display: flex;
        align-items: center;
        justify-content: ${(props) =>
            props.menuWidth ? 'center' : 'flex-start'};
        width: 100%;
        height: 30px;
        padding: 0 15px;
    }
    .app--menu li .app--menu-action:hover .app--menu-span,
    .app--menu li .app--menu-action:hover .app--menu-icon {
        color: var(--color-primary);
        fill: var(--color-primary);
    }
    .app--menu .app--menu-span {
        display: ${(props) => (props.menuWidth ? 'none' : 'inline-block')};
        padding: 0px;
        margin: 0px;
        margin-left: 15px;
        font-family: 'Nunito', sans-serif;
        font-size: 15px;
        font-weight: 600;
        color: #e4e4e9; /* rgb(208, 190, 208); */
        transition: color 0.3s linear;
    }
    .app--menu .app--menu-action .app--menu-icon {
        transition: fill 0.3s linear;
        fill: #d8dde7;
    }
    .app--menu-active {
        // background: rgba(0, 0, 0, 0.5);
    }
    .app--menu-active:before {
        background: var(--color-primary);
        content: '';
        transition: background 0.6s ease 0s;
        position: absolute;
        left: 0px;
        width: 3px;
        border-radius: 2px;
        height: inherit;
    }
    .app--menu-spanactive {
        color: var(--color-primary) !important;
        fill: var(--color-primary) !important;
    }
    .app--menu-borderactive {
        border-bottom: 2px solid #fafbfc;
        border-top: 2px solid #fafbfc;
        background: #fff;
    }
    .app--menu-borderactive:before {
        background: #fff;
        content: '';
        position: absolute;
        right: -2px;
        width: 2px;
        height: calc(100% - 1px);
    }
`;
