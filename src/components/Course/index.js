import { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import Pagination from '@material-ui/lab/Pagination';
import Skeleton from '../Skeleton';

import {
    CourseContainer,
    CourseSearchArea,
    CourseSearchContent,
    CourseAreaList,
    CoursePagination,
} from './course-style';

import CardCourse from '../CardCourse';
import { ButtonAddCourse } from '../Admin/ButtonSeetingCourse';
import { unLockAdmin } from '../../helpers/auth-unlock-admin';

import api from '../../services/api';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}));
const useQueryString = () => new URLSearchParams(useLocation().search);

let timer;

const Course = () => {
    const history = useHistory();
    const query = useQueryString();
    const classes = useStyles();
    const isAdmin = unLockAdmin();
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(parseInt(query.get('page'), 10));
    const [search, setSearch] = useState(query.get('search'));
    const [count, setCount] = useState(0);
    const [listCourse, setListCourse] = useState({});
    const [loadingPrimary, setLoadingPrime] = useState(false);

    document.title = 'Meus cursos';

    const handleChange = (event, value) => {
        setPage(value);
        history.push(`cursos?page=${value}&search=${search}`);
    };

    const catchCourse = async () => {
        setLoading(false);
        const course = await api.getCourse('course', {
            page: page || 1,
            search: search || '',
        });
        setListCourse(course.courseArray);
        setCount(course.pages);
        setLoading(true);
    };

    const handleSearch = (event) => {
        if (search.trim() !== event.currentTarget.value.trim()) {
            setLoading(false);
            setSearch(event.currentTarget.value.trim());
        }
        history.push(
            `cursos?page=1&search=${event.currentTarget.value.toLowerCase()}`
        );
    };

    useEffect(() => {
        catchCourse();
        setLoadingPrime(true);
    }, [page]);

    useEffect(() => {
        if (timer) clearTimeout(timer);
        if (search || loadingPrimary) timer = setTimeout(catchCourse, 2000);
    }, [search]);

    return (
        <CourseContainer>
            <CourseSearchArea>
                <CourseSearchContent>
                    <SearchIcon
                        style={{ marginLeft: '15px', color: '#d8dde7' }}
                    />
                    <input
                        type="search"
                        name="course"
                        placeholder="Qual curso você procura?"
                        className="input--search"
                        spellCheck="false"
                        autoFocus={true} // eslint-disable-line
                        autoComplete="off"
                        onChange={handleSearch}
                    />
                </CourseSearchContent>
            </CourseSearchArea>

            <CourseAreaList>
                {isAdmin && <ButtonAddCourse />}
                {!loading && (
                    <>
                        <Skeleton variant="rect" width="340" height="250" />
                        <Skeleton variant="rect" width="340" height="250" />
                        <Skeleton variant="rect" width="340" height="250" />
                        <Skeleton variant="rect" width="340" height="250" />
                        <Skeleton variant="rect" width="340" height="250" />
                        <Skeleton variant="rect" width="340" height="250" />
                    </>
                )}
                {loading &&
                    listCourse.map((item, key) => (
                        <CardCourse
                            slug={item.slug}
                            id={item.id}
                            title={item.title}
                            concluded={item.qt_concluded}
                            modules={item.qt_disciplines}
                            image={item.figure}
                            key={key} // eslint-disable-line
                        />
                    ))}
            </CourseAreaList>
            <CoursePagination>
                <div className={classes.root}>
                    <Pagination
                        count={count}
                        variant="outlined"
                        color="primary"
                        onChange={handleChange}
                        page={page || 1}
                        style={{ margin: '0px' }}
                    />
                </div>
            </CoursePagination>
        </CourseContainer>
    );
};

export default Course;
