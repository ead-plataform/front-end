import styled from 'styled-components';

export const CourseContainer = styled.div`
    width: 100%;
    height: calc(100vh - 70px);
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    padding: 15px;
`;

export const CourseSearchArea = styled.div`
    width: 100%;
    max-width: 1120px;
    height: 58px;
    display: flex;
    justify-content: center;
    background: #fff;
    margin-top: 20px;
    border-radius: 5px;
    box-shadow: rgb(0 0 0 / 5%) 0px 6px 12px;
`;

export const CourseSearchContent = styled.div`
    width: 100%;
    max-width: 1120px;
    display: flex;
    align-items: center;
    height: inherit;
    padding: 10px 0px;
    justify-content: space-around;

    .input--search {
        height: 40px;
        width: 95%;
        outline: none;
        flex: 1;
        border-radius: 5px;
        border: none;
        padding: 10px;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 400;
        color: var(--color-secundary);
    }

    .input--search::placeholder {
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        fons-size: 14px;
    }
`;

export const CourseAreaList = styled.div`
    width: 100%;
    max-width: 1120px;
    flex: 1;
    padding: 40px 0;
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;

    @media only screen and (max-width: 575px) {
        & {
            justify-content: center;
        }
        a {
            width: 100%;
            max-width: none;
        }
    }
    @media only screen and (max-width: 560px) {
        & > div {
            max-width: 100%;
        }
    }
`;

export const CoursePagination = styled.div`
    width: 100%;
    max-width: 1120px;
    display: flex;
    justify-content: center;
    padding: 10px;
    height: 100px;
`;
