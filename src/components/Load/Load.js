import style from 'styled-components';

export const Loading = style.div`

width:85%;

.lds-ripple {
    display: inline-block;
    position: relative;
    height: 0px;
    top: -25px;
  }
  .lds-ripple div {
    position: absolute;
    border: 4px solid #fff;
    opacity: 1;
    border-radius: 50%;
    box-sizing:content-box;
    animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
  }
  .lds-ripple div:nth-child(2) {
    animation-delay: -0.5s;
  }
  @keyframes lds-ripple {
    0% {
      top: 16px;
      left: 16px;
      width: 0;
      height: 0;
      opacity: 1;
    }
    100% {
      top: 0px;
      left: 0px;
      width: 32px;
      height: 32px;
      opacity: 0;
    }
  }
  
  
`;
