import { Loading } from './Load';

export default () => (
    <>
        <Loading>
            <div className="lds-ripple">
                <div />
                <div />
            </div>
        </Loading>
    </>
);
