import { useState } from 'react';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import {
    ProfilePassContainer,
    ProfilePassTitle,
    ProfilePassBody,
} from './profile-pass';
import { Button } from '../Button';
import api from '../../services/api';
import AlertMessage from '../AlertMessage';

export default () => {
    const [view, setViewPass] = useState(true);
    const [viewNew, setViewNew] = useState(true);
    const [viewConfirm, setViewConfirm] = useState(true);
    const [currentPassword, setCurrentPassword] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [message, setMessage] = useState({});

    const [messageAlert, setMessageAlert] = useState('');

    const handleViewPass = () => {
        setViewPass(!view);
    };
    const handleNewView = () => {
        setViewNew(!viewNew);
    };
    const handleViewConfirm = () => {
        setViewConfirm(!viewConfirm);
    };

    const handleChangePassword = async () => {
        if (password === confirmPassword) {
            const changePassword = await api.changePassword('change-password', {
                password,
                confirmPassword,
                currentPassword,
            });
            if (changePassword.success) {
                setMessageAlert(changePassword.success);
                setMessage({
                    variant: 'filled',
                    severity: 'success',
                    show: true,
                });
                setTimeout(() => {
                    setMessage({ show: false });
                }, 2500);
                setCurrentPassword('');
                setPassword('');
                setConfirmPassword('');
            } else {
                setMessageAlert(changePassword.error);
                setMessage({
                    variant: 'filled',
                    severity: 'error',
                    show: true,
                });
                setTimeout(() => {
                    setMessage({ show: false });
                }, 2500);
            }
        } else {
            setMessageAlert('as senhas não correspondem');
            setMessage({
                variant: 'filled',
                severity: 'error',
                show: true,
            });
            setTimeout(() => {
                setMessage({ show: false });
            }, 2500);
        }
    };

    return (
        <ProfilePassContainer>
            <AlertMessage
                variant={message.variant}
                severity={message.severity}
                show={message.show}
            >
                {messageAlert}
            </AlertMessage>
            <ProfilePassTitle>Alterar senha</ProfilePassTitle>
            <ProfilePassBody>
                <div className="col--group">
                    <input
                        type={view ? 'password' : 'text'}
                        name="password"
                        placeholder="Senha Atual"
                        value={currentPassword}
                        onChange={(e) =>
                            setCurrentPassword(e.currentTarget.value)
                        }
                    />
                    {view && (
                        <VisibilityIcon
                            fontSize="small"
                            onClick={handleViewPass}
                        />
                    )}
                    {!view && (
                        <VisibilityOffIcon
                            fontSize="small"
                            onClick={handleViewPass}
                        />
                    )}
                </div>
                <div className="col--group">
                    <input
                        type={viewNew ? 'password' : 'text'}
                        name="newpass"
                        placeholder="Nova senha"
                        value={password}
                        onChange={(e) => setPassword(e.currentTarget.value)}
                    />
                    {viewNew && (
                        <VisibilityIcon
                            fontSize="small"
                            onClick={handleNewView}
                        />
                    )}
                    {!viewNew && (
                        <VisibilityOffIcon
                            fontSize="small"
                            onClick={handleNewView}
                        />
                    )}
                </div>
                <div className="col--group">
                    <input
                        type={viewConfirm ? 'password' : 'text'}
                        name="newpassconfirm"
                        placeholder="Confirmar Senha"
                        value={confirmPassword}
                        onChange={(e) =>
                            setConfirmPassword(e.currentTarget.value)
                        }
                    />
                    {viewConfirm && (
                        <VisibilityIcon
                            fontSize="small"
                            onClick={handleViewConfirm}
                        />
                    )}
                    {!viewConfirm && (
                        <VisibilityOffIcon
                            fontSize="small"
                            onClick={handleViewConfirm}
                        />
                    )}
                </div>
                <div className="col--group button--group">
                    <Button color="#595bf9" onClick={handleChangePassword}>
                        Salvar Alterações
                    </Button>
                </div>
            </ProfilePassBody>
        </ProfilePassContainer>
    );
};
