import styled from 'styled-components';

export const ProfilePassContainer = styled.div`
    width: 65%;
    height: max-content;
    display: flex;
    flex-direction: column;
    padding: 10px;
    box-shadow: rgb(0 0 0 / 10%) 0px 1px 2px;

    @media only screen and (max-width: 930px) {
        width: 100%;
        margin-top: 30px;
        margin-bottom: 30px;
    }
`;

export const ProfilePassTitle = styled.strong`
    font-size: smaller;
    font-family: 'Nunito', sans-serif;
`;

export const ProfilePassBody = styled.div`
    padding-top: 20px;

    .col--group {
        position: relative;
        display: flex;
        align-items: center;
        margin-bottom: 20px;

        input {
            flex: 1;
            padding: 10px;
            border: 1px solid rgb(221, 221, 223);
            outline-color: var(--color-primary);
            font-family: 'Roboto', sans-serif;
            border-radius: 4px;
        }
        input::placeholder {
            font-size: small;
        }
        svg {
            position: absolute;
            fill: #d8dde7;
            right: 10px;
            cursor: pointer;
        }
    }
    .button--group {
        margin-top: 40px;
    }
`;
