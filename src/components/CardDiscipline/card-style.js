import styled from 'styled-components';

export const Card = styled.div`
    width: 100%;
    max-width: 210px;
    height: 150px;
    background: tomato;
    display: flex;
    flex-direction: column;
    border-radius: 5px;
    background: ${(props) => props.top};
    margin-right: 30px;
    margin-bottom: 30px;

    a {
        height: 100%;
    }

    @media only screen and (max-width: 740px) {
        & {
            width: 40%;
            max-width: none;
        }
    }
    @media only screen and (max-width: 450px) {
        & {
            width: 100%;
            justify-content: center;
            margin: 0 0 30px 0;
        }
    }
`;

export const CardContainer = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
`;

export const CardContentTop = styled.div`
    width: 100%;
    flex: 1;
    padding: 15px;
    display: flex;
    flex-direction: column;

    .header--name,
    .header--module {
        font-family: 'Nunito', sans-serif;
        font-size: smaller;
        color: rgb(37, 39, 45);
    }
    .header--name {
        margin-bottom: 10px;
        display: flex;
        justify-content: space-between;
    }
    .header--progress {
        width: 100%;
        margin-top: 20px;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .header--progress .header-progress-porcentu {
        width: 75%;
        height: 5px;
        background: #d8dde7;
        border-radius: 3px;
    }
    .header--progress .header-progress-porcentu div {
        width: 40%;
        height: inherit;
        border-radius: 3px;
        background: var(--color-secundary);
    }
    .header--progress .header--progress-span {
        font-family: 'Nunito', sans-serif;
        font-weight: 600;
        font-size: smaller;
    }
`;

export const CardContentBottom = styled.div`
    width: 100%;
    min-height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: ${(props) => props.bottom};
    color: #fff;
    font-family: 'Nunito', sans-serif;
    font-size: small;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    cursor: pointer;
`;
