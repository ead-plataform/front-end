import { Link } from 'react-router-dom';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import CreateOutlinedIcon from '@material-ui/icons/CreateOutlined';
import { AppUseValue } from '../../contexts/AppContext';
import { unLockAdmin } from '../../helpers/auth-unlock-admin';

import {
    Card,
    CardContainer,
    CardContentTop,
    CardContentBottom,
} from './card-style';

export default ({ name, modules, top, bottom, slug, id, watching }) => {
    const [state, dispatch] = AppUseValue(); // eslint-disable-line
    const isAdmin = unLockAdmin();
    const porcentagem = (modules / 100) * watching;
    const handleDiscipline = () => {
        dispatch({ type: 'iddiscipline', id });
        dispatch({ type: 'disciplinetitle', title: name });
        dispatch({ type: 'slugdiscipline', slug });
        dispatch({ type: 'porcetu', percentu: Math.ceil(porcentagem) });
        dispatch({ type: 'discqtmodule', qt_modules: modules });
        dispatch({ type: 'discqconclu', qt_concluded: watching });
    };

    const handleDelete = (event) => {
        event.preventDefault();
    };

    return (
        <>
            <Card top={top} onClick={handleDiscipline}>
                <Link to={`${window.location.pathname}/${slug}`}>
                    <CardContainer>
                        <CardContentTop>
                            <span className="header--name">
                                {name}
                                {isAdmin && (
                                    <>
                                        <DeleteForeverIcon
                                            style={{ color: '#F44336' }}
                                            onClick={handleDelete}
                                        />
                                        <CreateOutlinedIcon />
                                    </>
                                )}
                            </span>
                            <span className="header--module">
                                {modules} módulos
                            </span>
                        </CardContentTop>
                        <CardContentBottom bottom={bottom}>
                            <span>Ver módulos</span>
                        </CardContentBottom>
                    </CardContainer>
                </Link>
            </Card>
        </>
    );
};
