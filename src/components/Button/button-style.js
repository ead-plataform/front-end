import styled from 'styled-components';

export const ButtonContent = styled.button`
    width: 100%;
    display: block;
    cursor: pointer;
    text-transform: uppercase;
    border-radius: 5px;
    background: ${(props) => props.color};
    padding: 12px 32px;
    transition: background, color 0.3s linear;
    font-size: 14px;
    text-align: center;
    border: 2px solid #6c63ff;
    font-family: 'Roboto', sans-serif;
    color: #e1e1e6;
    outline: none;

    &:hover {
        background: var(--color-link);
        color: #fff;
    }
`;
