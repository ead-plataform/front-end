import { ButtonContent } from './button-style';

export const Button = ({ children, color, onClick, style }) => (
    <>
        <ButtonContent color={color} onClick={onClick} style={style}>
            {children}
        </ButtonContent>
    </>
);
