import { Alert } from '@material-ui/lab';
import { AlertContainer } from './alert-style';

const AlertMessage = ({ children, variant, severity, show }) => (
    <AlertContainer show={show}>
        <Alert
            variant={variant}
            severity={severity}
            style={{
                display: show ? 'flex' : 'none',
            }}
        >
            {children}
        </Alert>
    </AlertContainer>
);

export default AlertMessage;
