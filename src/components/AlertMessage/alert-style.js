import styled from 'styled-components';

export const AlertContainer = styled.div`
    width: 100%;
    height: max-content;
    position: absolute;
    padding: 25px 10px;
    left: 0px;
    top: 0px;
    display: ${(props) => (props.show ? 'flex' : 'none')};
    justify-content: center;
`;
