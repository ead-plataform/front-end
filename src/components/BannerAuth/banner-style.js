import styled from 'styled-components';

export const BannerContainer = styled.div`
    width: 50%;
    height: 100vh;

    @media only screen and (max-width: 1040px) {
        display: none;
    }
`;

export const Banner = styled.div`
    width: 80%;
    height: 100vh;
`;

export const BannerFigure = styled.img`
    width: 35%;
    position: absolute;
    top: 20%;
    left: 7%;
    user-select: none;
`;
