import { BannerContainer, Banner, BannerFigure } from './banner-style';

export const BannerAuth = () => (
    <>
        <BannerContainer>
            <Banner>
                <svg
                    version="1.1"
                    id="Camada_1"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="#595bf9"
                    style={{ height: '101vh' }}
                    x="0px"
                    y="0px"
                    viewBox="0 0 1400 1700"
                    xmlSpace="preserve"
                >
                    <style type="text/css" />
                    <g transform="translate(0.000000,1700.000000) scale(0.100000,-0.100000)">
                        <path
                            className="st0"
                            d="M0,8502V0h4838c2315.8,20.9,4631.4,25.9,6947,15c726.6-3.4,1453.2-8.4,2179.7-15c-1.6,13.7-3.1,27.3-4.7,41v60
                        c0,84-36,420-66,623c-189,1265-615,2507-1220,3557c-756,1312-1805,2353-3086,3063c-463,256-975,478-1493,646
                        c-154,51-410,134-570,186c-2465,803-4575,2701-6140,5524c-534,963-1067,2218-1330,3130c-26,91-49,167-51,169S0,13178,0,8502z"
                        />
                    </g>
                </svg>
            </Banner>
            <BannerFigure src="assets/image/book.svg" />
        </BannerContainer>
    </>
);
