import { useState } from 'react';
import InputMask from 'react-input-mask';
import MaterialInput from '@material-ui/core/Input';
import {
    ProfileContainer,
    ProfileHeader,
    ProfileTitle,
    ProfileBody,
} from './profile-style';

import { Button } from '../Button';
import api from '../../services/api';

import { AppUserValue } from '../../contexts/UserContext';
import AlertMessage from '../AlertMessage';

export default () => {
    const [state] = AppUserValue();
    const [email, setEmail] = useState(state.user.email);
    const [name, setName] = useState(state.user.name);
    const [phone, setPhone] = useState(state.user.phone);
    const [cep, setCep] = useState(state.user.zipcode);
    const [address, setAddress] = useState(state.user.address);
    const [number, setNumber] = useState(state.user.number);
    const [bairro, setBairro] = useState(state.user.bairro);
    const [city, setCity] = useState(state.user.city);
    const [states, setState] = useState(state.user.state);
    const [message, setMessage] = useState({});
    const [messageAlert, setMessageAlert] = useState('');

    const handleUpdate = async (event) => {
        event.preventDefault();
        await api.changedUsers('user', {
            name,
            phone,
            cep,
            address,
            number,
            district: bairro,
            city,
            state: states,
        });
        setMessageAlert('Suas informações foram salvas');
        setMessage({
            variant: 'filled',
            severity: 'success',
            show: true,
        });
        setTimeout(() => {
            setMessage({ show: false });
        }, 2500);
    };

    return (
        <>
            <AlertMessage
                variant={message.variant}
                severity={message.severity}
                show={message.show}
            >
                {messageAlert}
            </AlertMessage>
            <ProfileContainer>
                <ProfileHeader>
                    <ProfileTitle>Minhas informações</ProfileTitle>
                </ProfileHeader>
                <ProfileBody>
                    <div className="col--group">
                        <input
                            type="text"
                            placeholder="Nome"
                            className="input--name"
                            onChange={(e) => setName(e.currentTarget.value)}
                            value={name}
                        />
                    </div>
                    <div className="col--group">
                        <input
                            type="email"
                            placeholder="meuemail@gmail.com"
                            className="input--email"
                            onChange={(e) => setEmail(e.currentTarget.value)}
                            value={email}
                            disabled
                        />

                        <InputMask
                            mask="(99) 999999999"
                            maskChar={null}
                            value={phone}
                            onChange={(e) => setPhone(e.currentTarget.value)}
                            placeholder="(##) #########"
                            className="number"
                        />
                    </div>

                    <div className="col--group">
                        <input
                            type="text"
                            placeholder="Cep"
                            className="input--cep"
                            onChange={(e) => setCep(e.currentTarget.value)}
                            value={cep}
                        />
                        <input
                            type="text"
                            placeholder="Endereço"
                            className="input--address"
                            onChange={(e) => setAddress(e.currentTarget.value)}
                            value={address}
                        />
                        <input
                            type="text"
                            placeholder="Nº"
                            className="input--number"
                            onChange={(e) => setNumber(e.currentTarget.value)}
                            value={number}
                        />
                    </div>
                    <div className="col--group">
                        <input
                            type="text"
                            placeholder="bairro"
                            className="input--bairro"
                            onChange={(e) => setBairro(e.currentTarget.value)}
                            value={bairro}
                        />
                    </div>
                    <div className="col--group">
                        <input
                            type="text"
                            placeholder="cidade"
                            className="input--city"
                            onChange={(e) => setCity(e.currentTarget.value)}
                            value={city}
                        />
                        <input
                            type="text"
                            placeholder="UF"
                            className="input--uf"
                            onChange={(e) => setState(e.currentTarget.value)}
                            value={states}
                        />
                    </div>
                    <div className="col--group">
                        <Button color="#595bf9" onClick={handleUpdate}>
                            Salvar Informações
                        </Button>
                    </div>
                </ProfileBody>
            </ProfileContainer>
        </>
    );
};
