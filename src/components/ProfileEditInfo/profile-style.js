import styled from 'styled-components';

export const ProfileContainer = styled.div`
    width: 65%;
    display: flex;
    flex-direction: column;
    box-shadow: rgb(0 0 0 / 10%) 0px 1px 2px;

    @media only screen and (max-width: 930px) {
        width: 100%;
        margin-top: 30px;
        margin-bottom: 30px;
    }
`;

export const ProfileHeader = styled.div`
    width: 100%;
    padding: 0px 10px;
    display: flex;
`;

export const ProfileTitle = styled.strong`
    font-size: smaller;
    font-family: 'Nunito', sans-serif;
    padding-top: 15px;
`;

export const ProfileBody = styled.div`
    width: 100%;
    padding: 20px 10px;

    .col--group > input[disabled] {
        cursor: not-allowed;
    }

    .col--group {
        width: 100%;
        display: flex;
        margin-bottom: 20px;
        justify-content: space-between;

        & input {
            padding: 10px;
            border: 1px solid rgb(221, 221, 223);
            outline-color: var(--color-primary);
            font-family: 'Roboto', sans-serif;
            border-radius: 4px;
            font-size: 15px;
        }
        & input::placeholder {
            font-family: 'Roboto', sans-serif;
            font-size: small;
        }

        & .input--name {
            width: 100%;
        }
        & .input--email,
        .input--cpf,
        .input--phone {
            width: 45%;
        }
        & .input--cep {
            width: 25%;
        }
        & .input--address {
            width: 35%;
        }
        & .input--number {
            width: 25%;
        }
        & .input--complement {
            width: 45%;
        }
        & .input--bairro {
            width: 45%;
        }
        & .input--city {
            width: 65%;
        }
        & .input--uf {
            width: 25%;
        }

        @media only screen and (max-width: 580px) {
            .input--email,
            .input--phone,
            .input--cpf {
                width: 100%;
            }
            .number {
                margin-top: 30px;
            }
            .input--cpf,
            .input--phone {
                margin-top: 20px;
            }
        }
    }
    @media only screen and (max-width: 580px) {
        & .col--group {
            flex-wrap: wrap;
        }
    }
`;
