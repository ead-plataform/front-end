import Dropzone from 'react-dropzone';
import { useState } from 'react';

import { DropZoneContainer, UploadMessage } from './drop-style';

export default ({ title }) => {
    const [file, setFile] = useState([]);

    console.log(title);

    const renderDragMessage = (dragActive, dragReject, onDropAccepted) => {
        if (!dragActive) {
            return <UploadMessage>Arraste seus arquivos aqui...</UploadMessage>;
        }
        if (dragReject) {
            return (
                <UploadMessage type="error">
                    Arquivo não suportado
                </UploadMessage>
            );
        }
        return <UploadMessage type="success">Solte os arquivos</UploadMessage>;
    };

    return (
        <Dropzone accept="image/*" onDropAccepted={() => {}}>
            {({
                getRootProps,
                getInputProps,
                isDragActive,
                isDragReject,
                acceptedFiles,
            }) => (
                <DropZoneContainer
                    {...getRootProps()}
                    isDragActive={isDragActive}
                    isDragReject={isDragReject}
                >
                    <input {...getInputProps()} />
                    {renderDragMessage(isDragActive, isDragReject)}
                    {setFile(acceptedFiles[0])}
                </DropZoneContainer>
            )}
        </Dropzone>
    );
};
