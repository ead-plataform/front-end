import { fade, makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useState } from 'react';
import Dropzone from 'react-dropzone';
import { DropZoneContainer, UploadMessage } from '../Upload/drop-style';

import { FormArea, FormContainer, FormContent } from './add-course';
import { Button } from '../../Button';
import api from '../../../services/api';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    align: {
        width: '100%',
        '@media only screen and (max-width: 1030px) ': {
            '&': {
                width: '100%',
                marginBottom: '15px',
            },
        },
    },
}));

const useStylesReddit = makeStyles((theme) => ({
    root: {
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 4,
        backgroundColor: '#fcfcfb',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },
    },
    focused: {},
}));

function RedditTextField(props) {
    const classes = useStylesReddit();

    return (
        <TextField
            InputProps={{ classes, disableUnderline: true }}
            {...props}
        />
    );
}

export default () => {
    const classes = useStyles();

    const [title, setTitle] = useState('');
    const [file, setFile] = useState();

    const renderDragMessage = (dragActive, dragReject, onDropAccepted) => {
        if (!dragActive) {
            return <UploadMessage>Arraste seus arquivos aqui...</UploadMessage>;
        }
        if (dragReject) {
            return (
                <UploadMessage type="error">
                    Arquivo não suportado
                </UploadMessage>
            );
        }
        return <UploadMessage type="success">Solte os arquivos</UploadMessage>;
    };

    const handleAddCourse = async (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('figure', file);
        formData.append('title', title);
        const addCourse = await api.addCourse('course/add', formData);
    };

    return (
        <FormArea>
            <FormContainer>
                <FormContent>
                    <form
                        className={classes.root}
                        noValidate
                        onSubmit={handleAddCourse}
                    >
                        <RedditTextField
                            label="Título"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            value={title}
                            onChange={(value) =>
                                setTitle(value.currentTarget.value)
                            }
                        />
                        <Dropzone accept="image/*" onDropAccepted={() => {}}>
                            {({
                                getRootProps,
                                getInputProps,
                                isDragActive,
                                isDragReject,
                                acceptedFiles,
                            }) => (
                                <DropZoneContainer
                                    {...getRootProps()}
                                    isDragActive={isDragActive}
                                    isDragReject={isDragReject}
                                >
                                    <input {...getInputProps()} />
                                    {renderDragMessage(
                                        isDragActive,
                                        isDragReject
                                    )}
                                    {setFile(acceptedFiles[0])}
                                </DropZoneContainer>
                            )}
                        </Dropzone>
                        <Button color="#595bf9" style={{ marginTop: '25px' }}>
                            Adicionar
                        </Button>
                    </form>
                </FormContent>
            </FormContainer>
        </FormArea>
    );
};
