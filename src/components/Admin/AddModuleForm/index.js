import { fade, makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { FormArea, FormContainer, FormContent } from './add-course';
import { Button } from '../../Button';
import api from '../../../services/api';
import { AppUseValue } from '../../../contexts/AppContext';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    align: {
        width: '45%',
        marginBottom: '20px',
        '@media only screen and (max-width: 1030px) ': {
            '&': {
                width: '100%',
                marginBottom: '15px',
            },
        },
    },
}));

const useStylesReddit = makeStyles((theme) => ({
    root: {
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 4,
        backgroundColor: '#fcfcfb',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },
    },
    focused: {},
}));

function RedditTextField(props) {
    const classes = useStylesReddit();

    return (
        <TextField
            InputProps={{ classes, disableUnderline: true }}
            {...props}
        />
    );
}

export default () => {
    const classes = useStyles();
    const [state] = AppUseValue();
    const [title, setTitle] = useState('');
    const [order, setOrder] = useState('');
    const [qtmaterials, setQtMaterials] = useState('');
    const history = useHistory();

    const handleAddModule = async (event) => {
        event.preventDefault();
        const module = await api.addModule('/module', {
            title,
            order,
            qtmaterials,
            disciplineId: state.course.discipline.id,
        });
        history.goBack();
    };

    return (
        <FormArea>
            <FormContainer>
                <FormContent>
                    <form
                        className={classes.root}
                        noValidate
                        onSubmit={handleAddModule}
                    >
                        <RedditTextField
                            label="Título"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            value={title}
                            onChange={(value) =>
                                setTitle(value.currentTarget.value)
                            }
                        />
                        <RedditTextField
                            label="Quantidade de aulas"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            value={qtmaterials}
                            onChange={(value) =>
                                setQtMaterials(value.currentTarget.value)
                            }
                        />
                        <RedditTextField
                            label="Ordenação"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            value={order}
                            onChange={(value) =>
                                setOrder(value.currentTarget.value)
                            }
                        />
                        <Button color="#595bf9" style={{ marginTop: '25px' }}>
                            Adicionar
                        </Button>
                    </form>
                </FormContent>
            </FormContainer>
        </FormArea>
    );
};
