import styled from 'styled-components';

export const FormArea = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
`;
export const FormContainer = styled.div`
    width: 80%;
    display: flex;
    align-items: center;
`;

export const FormContent = styled.div`
    width: 100%;
    height: calc(80%);
`;
