import { useState } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import api from '../../../services/api';
import { AppUseValue } from '../../../contexts/AppContext';
import { FormArea, FormContainer, FormContent } from './add-course';
import { Button } from '../../Button';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    align: {
        width: '45%',
        '@media only screen and (max-width: 1030px) ': {
            '&': {
                width: '100%',
                marginBottom: '15px',
            },
        },
    },
}));

const useStylesReddit = makeStyles((theme) => ({
    root: {
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 4,
        backgroundColor: '#fcfcfb',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },
    },
    focused: {},
}));

function RedditTextField(props) {
    const classes = useStylesReddit();

    return (
        <TextField
            InputProps={{ classes, disableUnderline: true }}
            {...props}
        />
    );
}

export default () => {
    const classes = useStyles();
    const [state] = AppUseValue();
    const [title, setTitle] = useState();
    const [order, setOrder] = useState();
    const [url, setUrl] = useState();

    const handleAddClass = async (event) => {
        event.preventDefault();
        const classe = await api.addClass('/classe', {
            title,
            order,
            url,
            moduleId: state.course.moduleId.id,
        });
    };

    return (
        <FormArea>
            <FormContainer>
                <FormContent>
                    <form
                        className={classes.root}
                        noValidate
                        onSubmit={handleAddClass}
                    >
                        <RedditTextField
                            label="Título"
                            className={classes.align}
                            variant="filled"
                            id="title-class"
                            value={title}
                            onChange={(value) =>
                                setTitle(value.currentTarget.value)
                            }
                        />
                        <RedditTextField
                            label="URL do video"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            value={url}
                            onChange={(value) =>
                                setUrl(value.currentTarget.value)
                            }
                        />
                        <RedditTextField
                            label="Ordernação"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            style={{ marginTop: '30px' }}
                            value={order}
                            onChange={(value) =>
                                setOrder(value.currentTarget.value)
                            }
                        />
                        <Button color="#595bf9" style={{ marginTop: '60px' }}>
                            Adicionar
                        </Button>
                    </form>
                </FormContent>
            </FormContainer>
        </FormArea>
    );
};
