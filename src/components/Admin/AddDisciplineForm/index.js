import { fade, makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useHistory } from 'react-router-dom';
import { useState } from 'react';

import { FormArea, FormContainer, FormContent } from './add-course';
import { Button } from '../../Button';
import { AppUseValue } from '../../../contexts/AppContext';
import api from '../../../services/api';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    align: {
        width: '45%',
        '@media only screen and (max-width: 1030px) ': {
            '&': {
                width: '100%',
                marginBottom: '15px',
            },
        },
    },
}));

const useStylesReddit = makeStyles((theme) => ({
    root: {
        border: '1px solid #e2e2e1',
        overflow: 'hidden',
        borderRadius: 4,
        backgroundColor: '#fcfcfb',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:hover': {
            backgroundColor: '#fff',
        },
        '&$focused': {
            backgroundColor: '#fff',
            boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
            borderColor: theme.palette.primary.main,
        },
    },
    focused: {},
}));

function RedditTextField(props) {
    const classes = useStylesReddit();

    return (
        <TextField
            InputProps={{ classes, disableUnderline: true }}
            {...props}
        />
    );
}

export default () => {
    const classes = useStyles();
    const [state] = AppUseValue();
    const [title, setTitle] = useState('');
    const [qtModules, setQtModules] = useState();
    const history = useHistory();

    const handleAddDiscipline = async (event) => {
        event.preventDefault();
        const discipline = await api.addDiscipline('/discipline', {
            title,
            qt_modules: qtModules,
            courseId: state.course.id,
        });
        history.goBack();
    };

    return (
        <FormArea>
            <FormContainer>
                <FormContent>
                    <form
                        className={classes.root}
                        noValidate
                        onSubmit={handleAddDiscipline}
                    >
                        <RedditTextField
                            label="Título"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            value={title}
                            onChange={(value) =>
                                setTitle(value.currentTarget.value)
                            }
                        />
                        <RedditTextField
                            label="Quantidade de Modulos"
                            className={classes.align}
                            variant="filled"
                            id="reddit-input"
                            value={qtModules}
                            onChange={(value) =>
                                setQtModules(value.currentTarget.value)
                            }
                        />
                        <Button color="#595bf9" style={{ marginTop: '25px' }}>
                            Adicionar
                        </Button>
                    </form>
                </FormContent>
            </FormContainer>
        </FormArea>
    );
};
