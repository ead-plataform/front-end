import styled, { css } from 'styled-components';

export const ButtonAdd = styled.button`
    width: 100%;
    max-width: ${(props) => props.width}px;
    height: ${(props) => props.height}px;
    border: 2px dashed #d8dde7;
    outline: none;
    padding: 0px;
    background: #fff;
    margin-bottom: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    flex-direction: column;
    margin-right: 30px;

    &:focus {
        border-color: var(--color-primary);
    }

    @media only screen and (max-width: 575px) {
        & {
            width: 100%;
            max-width: none;
        }
    }
`;

export const ButtonAddHeader = styled.div`
    width: 100%;
    height: 100px;
    border-bottom: 2px dashed #d8dde7;
    padding: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    ${(props) =>
        props.type === 'discipline' &&
        css`
            align-items: flex-start;
            justify-content: flex-start;
            flex-direction: column;
        `}
`;
export const ButtonAddBody = styled.div`
    flex: 1;
    width: 100%;
    padding: 20px;
    display: flex;
    flex-direction: column;
`;
export const ButtonAddBodyTitle = styled.strong`
    width: max-content;
    font-family: 'Roboto', sans-serif;
    font-size: ${(props) => props.title}px;
    font-weight: 500;
`;
export const ButtonAddPercentu = styled.div`
    width: 100%;
    align-items: center;
    display: flex;
    padding-top: 12px;
`;
export const ButtonAddProgress = styled.div`
    flex: 1;
    height: 8px;
    border-radius: 4px;
    border: 2px dashed #d8dde7;
`;
export const ButtonAddProgressTitle = styled.strong`
    width: 100%;
    max-width: 50px;
    display: block;
`;
export const ButtonCardAdd = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    transition: background 0.3s linear;

    svg {
        display: none;
    }

    &:hover {
        background: rgba(89 91 249 / 60%);
        svg {
            display: block;
        }
    }
`;
export const ButtonDesModule = styled.span`
    width: max-content;
    font-family: 'Roboto', sans-serif;
    font-size: small;
    padding: 8px 0;
`;
