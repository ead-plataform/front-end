import { useHistory } from 'react-router-dom';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
    ButtonAdd,
    ButtonAddHeader,
    ButtonAddBody,
    ButtonAddBodyTitle,
    ButtonAddPercentu,
    ButtonAddProgress,
    ButtonAddProgressTitle,
    ButtonCardAdd,
    ButtonDesModule,
} from './button-style';

export const ButtonAddCourse = () => {
    const history = useHistory();

    const handleAddCourse = () => {
        history.push('/app/admin/course/create');
    };

    return (
        <ButtonAdd onClick={handleAddCourse} width="350" height="207">
            <ButtonCardAdd>
                <AddCircleOutlineIcon style={{ fontSize: '50px' }} />
            </ButtonCardAdd>
            <ButtonAddHeader>
                <ButtonAddBodyTitle font="18">
                    Imagem do curso
                </ButtonAddBodyTitle>
            </ButtonAddHeader>
            <ButtonAddBody>
                <ButtonAddBodyTitle>Título do curso</ButtonAddBodyTitle>
                <ButtonAddPercentu>
                    <ButtonAddProgress />
                    <ButtonAddProgressTitle>0%</ButtonAddProgressTitle>
                </ButtonAddPercentu>
            </ButtonAddBody>
        </ButtonAdd>
    );
};

export const ButtonAddDiscipline = () => {
    const history = useHistory();

    const handleAddDiscipline = () => {
        history.push('/app/admin/course/discipline/create');
    };

    return (
        <ButtonAdd
            onClick={handleAddDiscipline}
            width="210"
            height="150"
            style={{ marginRight: '10px' }}
        >
            <ButtonCardAdd>
                <AddCircleOutlineIcon style={{ fontSize: '50px' }} />
            </ButtonCardAdd>
            <ButtonAddHeader type="discipline">
                <ButtonAddBodyTitle font="13">
                    Título disciplina
                </ButtonAddBodyTitle>
                <ButtonDesModule>qt modulos</ButtonDesModule>
                <ButtonAddPercentu>
                    <ButtonAddProgress />
                    <ButtonAddProgressTitle>0%</ButtonAddProgressTitle>
                </ButtonAddPercentu>
            </ButtonAddHeader>
            <ButtonAddBody
                style={{
                    padding: '0px',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <ButtonDesModule>ver módulos</ButtonDesModule>
            </ButtonAddBody>
        </ButtonAdd>
    );
};
