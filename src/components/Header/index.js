import { useState } from 'react';
import { Link } from 'react-router-dom';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { AppUseValue } from '../../contexts/AppContext';
import { AppUserValue } from '../../contexts/UserContext';
import {
    HeaderContainer,
    HeaderContainerContent,
    HeaderContentLeft,
    HeaderContentRight,
} from './header-style';

const Header = () => {
    const [showProfile, setProfile] = useState(false);
    const [users] = AppUserValue();
    const [name] = useState(users.user.name.split(' ').slice(0, 1));
    const [state, dispatch] = AppUseValue();

    const handleMenuOpen = () => {
        const menu = state.menu.menuOpened;
        dispatch({ type: 'setMenu', menuOpened: !menu });
    };

    const catchClick = (element) => {
        const html = document.documentElement;
        html.addEventListener('click', (event) => {
            if (!element.contains(event.target)) {
                setProfile(false);
            }
            if (event.target.classList.contains('header--options')) {
                setProfile(true);
            }
        });
    };

    const handleProfile = (profile) => {
        setProfile(!showProfile);
        catchClick(profile.currentTarget);
    };

    return (
        <HeaderContainer>
            <HeaderContainerContent>
                <HeaderContentLeft>
                    <div
                        className="header--content-left"
                        onClick={handleMenuOpen}
                        aria-hidden="true"
                    >
                        <span className="header--burguer-line" />
                    </div>
                </HeaderContentLeft>
                <HeaderContentRight profile={users.user.profile}>
                    <div className="header--notifications">
                        <NotificationsNoneIcon
                            style={{
                                color: '#d8dde7',
                                fontSize: '24px',
                            }}
                            className="header--notification-icon"
                        />
                    </div>
                    <div
                        className={`header--profile-configs ${
                            showProfile && 'open'
                        }`}
                        aria-hidden="true"
                        onClick={handleProfile}
                    >
                        <div className="header--name">
                            <p>{name}</p>
                        </div>
                        <div className="header--photo">
                            {users.user.profile === 'default.svg' && (
                                <PersonOutlineIcon
                                    style={{
                                        color: '#d8dde7',
                                        fontSize: '24px',
                                    }}
                                />
                            )}
                        </div>
                        <div className="header--arrow">
                            <KeyboardArrowDownIcon
                                style={{ color: '#d8dde7', fontSize: '24px' }}
                                className="header--notification-icon"
                            />
                            <div className="header--options">
                                <ul>
                                    <li>
                                        <Link to="/app/profile/me">
                                            Meu Perfil
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/app/exit">Sair</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </HeaderContentRight>
            </HeaderContainerContent>
        </HeaderContainer>
    );
};
export default Header;
