import styled from 'styled-components';

export const HeaderContainer = styled.header`
    width: 100%;
    display: flex;
    justify-content: center;
    height: 70px;
    padding: 0 15px;
    background-color: #fff;
    border-bottom: 2px solid #fafbfc;
`;

export const HeaderContainerContent = styled.div`
    width: 100%;
    max-width: 1120px;
    display: flex;
`;

export const HeaderContentLeft = styled.div`
    flex: 1;
    display: flex;
    align-items: center;

    .header--content-left {
        width: 100%;
        max-width: 50px;
        height: 50px;
        cursor: pointer;
        user-select: none;
    }
    .header--content-left:hover .header--burguer-line,
    .header--content-left:hover .header--burguer-line:before,
    .header--content-left:hover .header--burguer-line:after {
        background: var(--color-primary);
    }
    .header--burguer-line {
        display: block;
        width: 30px;
        height: 2px;
        background: #d8dde7;
        position: relative;
        top: 22px;
        left: 8px;
        transition: 0.3s ease-in-out;
    }
    .header--burguer-line:before,
    .header--burguer-line:after {
        content: '';
        background: #d8dde7;
        display: block;
        position: absolute;
        height: 85%;
        width: 100%;
        transition: 0.3s ease-in-out;
    }
    .header--burguer-line:before {
        top: -10px;
    }
    .header--burguer-line:after {
        bottom: -10px;
    }
`;

export const HeaderContentRight = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: flex-end;

    .header--notifications {
        width: max-content;
        padding: 5px;
        cursor: pointer;
        margin-right: 20px;
    }
    .header--notification-icon {
        transition: fill 0.3s linear;
    }
    .header--notification-icon:hover {
        fill: var(--color-primary);
    }
    .header--profile-configs {
        width: 100%;
        max-width: 140px;
        height: 35px;
        display: flex;
        align-items: center;
        justify-content: space-around;
        cursor: pointer;
        position: relative;
    }
    .header--photo {
        width: 35px;
        height: 35px;
        display: flex;
        align-items: center;
        justify-content: center;
        border: 2px solid #fafbfc;
        border-radius: 50%;

        background-image: url('${(props) =>
            props.profile !== 'default.svg' ? props.profile : ''}');
        background-size: cover;
        background-position: center;
    }
    .header--name p {
        margin: 0px;
        font-family: 'Roboto', sans-serif;
        text-transform: uppercase;
        font-size: small;
        color: #d8dde7;
    }
    .header--arrow {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .header--arrow .header--options {
        position: absolute;
        background: #fff;
        top: 80px;
        right: 0px;
        width: 100%;
        max-width: 200px;
        height: 92px;
        padding: 14px;
        transition: display 0.3s linear;
        box-shadow: rgb(0 0 0 / 5%) 0px 3px 12px;
        display: none;
        z-index: 4;
    }
    .open .header--arrow .header--options {
        display: block;
    }
    .header--arrow .header--options:before {
        content: '';
        display: block;
        background: #fff;
        position: absolute;
        width: 20px;
        height: 20px;
        top: -10px;
        right: 15px;
        transform: rotate(-45deg);
        box-shadow: rgb(0 0 0 / 2%) 5px -4px 6px 0px;
    }
    .header--options ul {
        display: flex;
        flex-direction: column;
    }
    .header--options ul li a {
        display: block;
        padding: 10px;
        font-family: 'Roboto', sans-serif;
        font-weight: 500;
        font-size: 14px;
        color: #d8dde7;
        transition: background 0.3s linear;
        z-index: 2;
    }
    .header--options ul li a:hover {
        background: #fafafc;
    }
`;
