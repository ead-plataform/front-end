import Cookie from 'js-cookie';

export const doLogin = () => {
    const token = Cookie.get('token');
    return !!token;
};

export const Login = (token) => {
    Cookie.set('token', token, { expires: 1 });
};

export const LogOut = () => {
    Cookie.remove('token');
    window.location.href = '/';
};
