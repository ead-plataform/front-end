import Cookie from 'js-cookie';
import { LogOut } from './auth-user';
import api from '../services/api';

export const LoginMultipleBlocked = async (priviting) => {
    const blocked = await api.blocked('locked', {});
    if (blocked !== Cookie.get('token')) {
        LogOut();
    }
};
