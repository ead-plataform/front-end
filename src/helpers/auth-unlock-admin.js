import Cookie from 'js-cookie';
import jwt from 'jwt-decode';

export const unLockAdmin = () => {
    const token = Cookie.get('token');
    const decoded = jwt(token);
    return decoded.admin;
};
