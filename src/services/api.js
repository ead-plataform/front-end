import Cookie from 'js-cookie';
import qs from 'qs';

const connect = {
    baseURL: 'https://app.cerebronerd.com.br/api',
};

const cookie = Cookie.get('token');

const options = {
    headers: {
        'Accept': 'application/json', // eslint-disable-line
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'x-access-token': `${cookie}`, // eslint-disable-line
    },
};

const methodTypeFile = async (router, body) => {
    const res = await fetch(`${connect.baseURL}/${router}`, {
        method: 'POST',
        headers: {
            'x-access-token': `${cookie}`,
        },
        body,
    });
    const json = await res.json();
    return json;
};

const methodPost = async (dados, router) => {
    const process = await fetch(`${connect.baseURL}${router}`, {
        method: 'POST',
        ...options,
        body: JSON.stringify({ ...dados }),
    });
    const result = await process.json();
    return result;
};

const methodPath = async (router, dados) => {
    const process = await fetch(`${connect.baseURL}/${router}`, {
        method: 'PATCH',
        ...options,
        body: JSON.stringify({ ...dados }),
    });
    const result = await process.json();
    return result;
};

const methodGet = async (router, body) => {
    const res = await fetch(
        `${connect.baseURL}/${router}?${qs.stringify(body)}`,
        {
            method: 'GET',
            ...options,
        }
    );
    const json = await res.json();
    return json;
};

class Connecting {
    async signin(dados, router) { // eslint-disable-line
        try {
            const signin = await methodPost(dados, router);
            return signin;
        } catch (err) {
            return err;
        }
    }

    async signup(dados, router) { // eslint-disable-line
        try {
            const signup = await methodPost(dados, router);
            return signup;
        } catch (err) {
            return err;
        }
    }

    async resetPassword(dados, router) { // eslint-disable-line
        const password = await methodPost(dados, router);
        return password;
    }

    async forgotPassword(dados, router) { // eslint-disable-line
        const forgot = await methodPost(dados, router);
        return forgot;
    }

    async checkingEmailSignup(dados, router) { // eslint-disable-line
        const checking = await fetch(
            `${connect.baseURL}/${router}?token=${dados}`,
            {
                method: 'GET',
                ...options,
            }
        );
        const isActive = await checking.json();
        return isActive;
    }

    async getCourse(router, body) { // eslint-disable-line
        try {
            const course = await methodGet(router, body);
            return course;
        } catch (err) {
            return err;
        }
    }

    async catchDiscipline(router, body) { // eslint-disable-line
        const disciplines = await fetch(
            `${connect.baseURL}/${router}?courseId=${body}`,
            {
                method: 'GET',
                ...options,
            }
        );
        const discipline = await disciplines.json();
        return discipline;
    }
    async catchModule(router, body) { // eslint-disable-line
        const modules = await fetch(
            `${connect.baseURL}/${router}?disciplineId=${body}`,
            {
                method: 'GET',
                ...options,
            }
        );
        const module = await modules.json();
        return module;
    }

    async catchProgressModule(router, body) { // eslint-disable-line
        const modules = await fetch(
            `${connect.baseURL}/${router}?disciplineId=${body}`,
            {
                method: 'GET',
                ...options,
            }
        );
        const module = await modules.json();
        return module;
    }

    async catchClassAndMaterials(router, body) { // eslint-disable-line
        const modules = await fetch(`${connect.baseURL}/${router}/${body}`, {
            method: 'GET',
            ...options,
        });
        const module = await modules.json();
        return module;
    }

    async catchClass(router, body) { // eslint-disable-line
        const modules = await fetch(`${connect.baseURL}/${router}/${body}`, {
            method: 'GET',
            ...options,
        });
        const classe = await modules.json();
        return classe;
    }

    async setProgressModule(router, body) { // eslint-disable-line
        try {
            const progress = await methodPost(body, router);
            return progress;
        } catch (err) {
            return err;
        }
    }
    async addHistoric(router, body) { // eslint-disable-line
        try {
            const progress = await methodPost(body, router);
            return progress;
        } catch (err) {
            return err;
        }
    }

    async addCourse(router, body) { // eslint-disable-line
        try {
            const progress = await methodTypeFile(router, body);
            return progress;
        } catch (err) {
            return err;
        }
    }

    async addDiscipline(router, body) { // eslint-disable-line
        try {
            const discipline = await methodPost(body, router);
            return discipline;
        } catch (err) {
            return err;
        }
    }

    async addModule(router, body) { // eslint-disable-line
        try {
            const modules = await methodPost(body, router);
            return modules;
        } catch (err) {
            return err;
        }
    }

    async addClass(router, body) { // eslint-disable-line
        try {
            const classe = await methodPost(body, router);
            return classe;
        } catch (err) {
            return err;
        }
    }

    async setProfile(router, body) { // eslint-disable-line
        try {
            const profile = await methodTypeFile(router, body);
            return profile;
        } catch (error) {
            return error;
        }
    }
    async changePassword(router, body) { // eslint-disable-line
        try {
            const profile = await methodPath(router, body);
            return profile;
        } catch (error) {
            return error;
        }
    }
    async changedUsers(router, body) { // eslint-disable-line
        try {
            const userInfo = await methodPath(router, body);
            return userInfo;
        } catch (error) {
            return error;
        }
    }

    async blocked(router, body) { // eslint-disable-line
        try {
            const blocked = await methodGet(router, body);
            return blocked;
        } catch (error) {
            return error;
        }
    }
}

export default new Connecting();
